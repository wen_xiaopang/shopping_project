/*
 Navicat Premium Data Transfer

 Source Server         : MySql
 Source Server Type    : MySQL
 Source Server Version : 80015
 Source Host           : localhost:3306
 Source Schema         : shopping

 Target Server Type    : MySQL
 Target Server Version : 80015
 File Encoding         : 65001

 Date: 17/03/2021 17:30:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for commodity
-- ----------------------------
DROP TABLE IF EXISTS `commodity`;
CREATE TABLE `commodity`  (
  `cid` int(11) NOT NULL AUTO_INCREMENT COMMENT '商品id',
  `cnumber` varbinary(20) NULL DEFAULT 0x3130303031 COMMENT '商品编号',
  `cname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品名',
  `cstatus` int(255) NULL DEFAULT 1 COMMENT '上下架状态',
  `introduction` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品介绍',
  `category` int(10) NULL DEFAULT NULL COMMENT '商品类型',
  `stock` int(255) NULL DEFAULT NULL COMMENT '库存',
  `cmoney` float(10, 2) NULL DEFAULT NULL COMMENT '商品单价',
  `discount` float(10, 2) NULL DEFAULT NULL COMMENT '折后价',
  `imgpath` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片路径',
  `amount` int(11) NULL DEFAULT 0 COMMENT '销量',
  `brand_id` int(11) NULL DEFAULT 1 COMMENT '品牌',
  `crowd_id` int(10) NULL DEFAULT 1 COMMENT '适应人群',
  `material_id` int(10) NULL DEFAULT 1 COMMENT '材料',
  `status_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '上架时间',
  PRIMARY KEY (`cid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 146 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of commodity
-- ----------------------------
INSERT INTO `commodity` VALUES (1, 0x3130303031, '太子龙牛仔夹克男', 1, '太子龙 TEDELON 夹克男立领纯色工装外套防风衣服男潮流休闲上衣时尚百搭夹克衫运动男装JQN J118 黑色 XL', 6, 100, 148.00, 141.00, 't_001.jpg', 0, 1, 1, 1, '2021-03-16 16:35:11');
INSERT INTO `commodity` VALUES (2, 0x3130303032, ' \r\n太子龙牛仔裤', 1, '太子龙（TEDELON）牛仔裤男裤子男工装裤时尚修身小脚男装系带束脚弹力潮男士休闲裤 MXK802 灰色 XL', 3, 100, 148.00, 138.00, 't_002.jpg', 0, 1, 1, 1, '2021-03-16 16:12:24');
INSERT INTO `commodity` VALUES (3, 0x3130303033, '太子龙短袖T恤', 1, '太子龙 TEDELON 短袖T恤男夏季男装衣服圆领个性潮流印花图案修身休闲舒适打底衫MYY2102 深灰 XL', 4, 100, 99.00, 93.00, 't_003.jpg', 0, 1, 1, 1, '2021-03-16 16:12:29');
INSERT INTO `commodity` VALUES (4, 0x3130303034, ' \r\n太子龙卫衣男', 1, '太子龙 TEDELON 卫衣男春秋季长袖T恤圆领拼色套头男士上衣时尚休闲潮流男装外套 YPY3096 灰色 XL', 2, 100, 138.00, 128.00, 't_004.jpg', 0, 1, 1, 1, '2021-03-16 16:12:35');
INSERT INTO `commodity` VALUES (5, 0x3130303035, '太子龙长袖衬衫男', 1, '太子龙 TEDELON 长袖衬衫男春季格子衫男士潮流时尚休闲上衣潮流条纹衬衫外套男装 KXP G37 深灰 XL', 2, 100, 138.00, 131.00, 't_005.jpg', 0, 1, 1, 1, '2021-03-16 16:12:38');
INSERT INTO `commodity` VALUES (6, 0x3130303036, '春秋季帅气黑色牛仔外套', 1, '南极人2021春秋季帅气黑色牛仔外套男韩版修身百搭夹克外套大码学生上衣服潮流 916-亮黑 XL', 3, 100, 158.00, NULL, 't_006.jpg', 0, 1, 1, 1, '2021-03-16 16:12:53');
INSERT INTO `commodity` VALUES (7, 0x3130303037, '新款黑色潮牌牛仔外套', 1, '南极人2021春秋新款黑色潮牌牛仔外套男宽松百搭潮流厚款休闲夹克男外套加肥加大码牛仔衣男 潮 黑色 XL', 3, 100, 168.00, 151.20, 't_007.jpg', 0, 1, 1, 1, '2021-03-16 16:13:16');
INSERT INTO `commodity` VALUES (8, 0x3130303038, '夏季棉质黑色短袖t恤', 1, '南极人夏季棉质黑色短袖t恤男宽松潮牌ins印花半袖大码胖子打底衫潮流体恤 黑色 XL', 4, 100, 89.00, 88.00, 't_008.jpg', 0, 1, 1, 1, '2021-03-16 16:13:30');
INSERT INTO `commodity` VALUES (9, 0x3130303039, '纯色棉质短袖t恤', 1, '南极人夏季潮牌纯色棉质短袖t恤男宽松潮流圆领胖子半袖大码男生T恤男装上衣潮 米黄色 L', 4, 100, 98.00, 90.00, 't_009.jpg', 0, 1, 1, 1, '2021-03-16 16:13:34');
INSERT INTO `commodity` VALUES (10, 0x3130303130, '新款休闲工装裤', 1, '南极人2021春夏季新款休闲工装裤男潮牌宽松束脚裤男哈伦运动韩版多口袋潮流修身长裤子男装 浅绿色 38', 3, 100, 168.00, 166.00, 't_010.jpg', 0, 1, 1, 1, '2021-03-16 16:13:45');
INSERT INTO `commodity` VALUES (11, 0x3130303131, 'Matri Mr牛仔夹克', 1, 'Matri Mr牛仔夹克男秋季新款宽松破洞牛仔外套男潮流韩版工装夹克百搭港风ins衣服 浅蓝色 L', 2, 100, 95.00, NULL, 't_011.jpg', 0, 1, 1, 1, '2021-03-16 16:07:30');
INSERT INTO `commodity` VALUES (12, 0x3130303132, '博谦 新款牛仔外套', 1, '博谦 2021春秋季新款牛仔外套男士夹克大码韩版休闲棉料学生潮流上衣修身青年刺绣男装bf风多口袋 TBK 819 M', 3, 100, 198.00, 158.00, 't_012.jpg', 0, 1, 1, 1, '2021-03-16 16:36:14');
INSERT INTO `commodity` VALUES (13, 0x3130303133, '男士春秋薄款茄克衫', 1, '富贵鸟外套男夹克男士春天秋季薄款茄克衫加绒加厚棉服中年爸爸装休闲大码男装立领棒球服棉袄衣服褂子 军绿薄款 XL(130-145斤)', 2, 100, 168.00, NULL, 't_013.jpg', 0, 1, 1, 1, '2021-03-16 16:14:00');
INSERT INTO `commodity` VALUES (14, 0x3130303134, '花花公子休闲夹克', 1, '花花公子休闲夹克男外套2021春季新款男士韩版潮牌多口袋立领夹克衫男装时尚潮流修身棒球服 2021黑色', 2, 100, 398.00, 269.00, 't_014.jpg', 0, 1, 1, 1, '2021-03-16 16:14:03');
INSERT INTO `commodity` VALUES (15, 0x3130303135, 'CROCODILE 夹克男', 1, '鳄鱼恤 CROCODILE 夹克男春秋新潮流休闲修身连帽外套男韩版帅气风衣男时尚运动男装 YF 1208 黑色 XL', 2, 100, 128.00, 121.00, 't_015.jpg', 0, 1, 1, 1, '2021-03-16 16:14:07');
INSERT INTO `commodity` VALUES (16, 0x3130303136, '花花公子皮衣男', 1, '花花公子（PLAYBOY）皮衣男2021春季韩版夹克男款休闲棒球领外套男士帅气机车皮衣男装 黑色', 3, 100, 298.00, 292.00, 't_016.jpg', 0, 1, 1, 1, '2021-03-16 16:14:12');
INSERT INTO `commodity` VALUES (17, 0x3130303137, '男士短袖套装T恤', 1, '花花公子旗舰夏季套装男潮流男装青少年宽松个性棉男士短袖T恤T 白灰', 1, 100, 299.00, 139.00, 't_017.jpg', 0, 1, 1, 1, '2021-03-16 16:14:40');
INSERT INTO `commodity` VALUES (18, 0x3130303138, '纯棉印花男士短袖t恤', 1, 'Soinku潮型库纯棉印花男士短袖t恤男圆领2020夏季潮流韩版学生五分袖体恤上衣潮牌半袖T恤衫 8328 白色', 4, 100, 68.00, 44.00, 't_018.jpg', 0, 1, 1, 1, '2021-03-16 16:14:35');
INSERT INTO `commodity` VALUES (19, 0x3130303139, '男士夏装短袖T恤', 1, '花花公子旗舰短袖T恤男2020新款夏季假两件男装衣服纯棉男士夏装 827-56黑色', 4, 100, 188.00, 90.00, 't_019.jpg', 0, 1, 1, 1, '2021-03-16 16:14:44');
INSERT INTO `commodity` VALUES (20, 0x3130303230, '套装男短袖T恤', 1, '花花公子旗舰套装男短袖T恤夏季潮流男士潮牌夏装学生青少年男装T 824-12深灰', 1, 100, 269.00, 169.00, 't_020.jpg', 0, 1, 1, 1, '2021-03-16 16:07:31');
INSERT INTO `commodity` VALUES (21, 0x3130303231, '卫衣男秋冬季外套', 0, '【两件装】南极人卫衣男2020秋冬季外套男新品加绒连帽打底衫男士韩版潮流长袖T恤一套运动卫衣套装男装 2061黑卡加绒款(卫衣+裤子)', 1, 100, 298.00, 121.00, 't_021.jpg', 0, 1, 1, 1, '2021-03-16 16:15:22');
INSERT INTO `commodity` VALUES (22, 0x3130303232, '潮型库连帽卫衣', 1, 'Soinku潮型库连帽卫衣男生韩版潮流学生帅气ins潮牌印花渐变休闲上衣服外套男装 6633卡灰色', 3, 100, 138.00, 91.00, 't_022.jpg', 0, 1, 1, 1, '2021-03-16 16:07:31');
INSERT INTO `commodity` VALUES (23, 0x3130303233, '春新款纯色棉质卫衣', 0, '安踏卫衣男装2021春新款纯色棉质舒适保暖上衣休闲运动套头衫长袖T恤内搭秋衣', 2, 100, 134.00, 114.00, 't_023.jpg', 0, 1, 1, 1, '2021-03-16 16:15:33');
INSERT INTO `commodity` VALUES (24, 0x3130303234, '太子龙长袖T恤', 0, '太子龙 TEDELON 长袖T恤男春季男装衣服圆领个性潮流卫衣男印花图案修身休闲舒适打底上衣MYY5102 白色', 4, 100, 99.00, 94.00, 't_024.jpg', 0, 1, 1, 1, '2021-03-16 16:15:41');
INSERT INTO `commodity` VALUES (25, 0x3130303235, '卫衣男无帽圆领', 1, '花花公子卫衣男无帽圆领假两件新款青少年打底衫学生外穿长袖T恤衫 白色', 3, 100, 269.00, 139.00, 't_025.jpg', 0, 1, 1, 1, '2021-03-16 16:15:55');
INSERT INTO `commodity` VALUES (26, 0x3130303236, '吉普卫衣男春秋新款', 1, 'JEEP吉普卫衣男2021春秋新款男士长袖圆领T恤宽松大码纯色运动休闲时尚棉质中青年学生打底衫秋衣服 6527(黑色)', 2, 100, 134.00, 88.00, 't_026.jpg', 0, 1, 1, 1, '2021-03-16 16:15:58');
INSERT INTO `commodity` VALUES (27, 0x3130303237, '男山羊皮夹克', 1, '意大利风格品牌春季新款海宁真皮皮衣男山羊皮夹克外套特大码商务休闲中年男士爸爸装加棉正装 黑色单皮', 3, 100, 878.00, 800.00, 't_027.jpg', 0, 1, 1, 1, '2021-03-16 16:16:13');
INSERT INTO `commodity` VALUES (28, 0x3130303238, '恒源祥牛皮皮衣', 1, '恒源祥高档品牌男装头层牛皮皮衣男真皮男士外套中年休闲直筒商务西装领春季 典雅黑色', 3, 100, 12999.00, 12349.05, 't_028.jpg', 0, 1, 1, 1, '2021-03-16 16:07:31');
INSERT INTO `commodity` VALUES (29, 0x3130303239, '海宁皮革上衣 ', 1, ' 恒源祥真皮皮衣男士立领纯色休闲保暖潮流男装海宁皮革上衣 15905581 黑色', 3, 100, 1280.00, 1234.05, 't_029.jpg', 0, 1, 1, 1, '2021-03-16 16:07:31');
INSERT INTO `commodity` VALUES (30, 0x3130303330, '太子龙牛仔夹克男', 0, '太子龙 TEDELON 夹克男立领纯色工装外套防风衣服男潮流休闲上衣时尚百搭夹克衫运动男装JQN J118 黑色 XL', 6, 100, 148.00, 140.00, 't_001.jpg', 0, 1, 1, 1, '2021-03-16 18:17:16');
INSERT INTO `commodity` VALUES (31, 0x3130303331, '太子龙牛仔裤', 1, '太子龙（TEDELON）牛仔裤男裤子男工装裤时尚修身小脚男装系带束脚弹力潮男士休闲裤 MXK802 灰色 XL', 6, 80, 148.00, 138.00, 't_002.jpg', 0, 1, 1, 1, '2021-03-16 16:21:18');
INSERT INTO `commodity` VALUES (32, 0x3130303332, '太子龙短袖T恤', 0, '太子龙 TEDELON 短袖T恤男夏季男装衣服圆领个性潮流印花图案修身休闲舒适打底衫MYY2102 深灰 XL', 7, 100, 99.00, 93.00, 't_003.jpg', 0, 1, 1, 1, '2021-03-16 16:21:21');
INSERT INTO `commodity` VALUES (33, 0x3130303333, '太子龙卫衣男', 1, '太子龙 TEDELON 卫衣男春秋季长袖T恤圆领拼色套头男士上衣时尚休闲潮流男装外套 YPY3096 灰色 XL', 5, 100, 138.00, 128.00, 't_004.jpg', 0, 1, 1, 1, '2021-03-16 16:21:24');
INSERT INTO `commodity` VALUES (34, 0x3130303334, '太子龙长袖衬衫男', 1, '太子龙 TEDELON 长袖衬衫男春季格子衫男士潮流时尚休闲上衣潮流条纹衬衫外套男装 KXP G37 深灰 XL', 5, 100, 138.00, 131.00, 't_005.jpg', 0, 1, 1, 1, '2021-03-16 16:07:32');
INSERT INTO `commodity` VALUES (35, 0x3130303335, '春秋季帅气黑色牛仔外套', 1, '南极人2021春秋季帅气黑色牛仔外套男韩版修身百搭夹克外套大码学生上衣服潮流 916-亮黑 XL', 6, 100, 158.00, NULL, 't_006.jpg', 0, 1, 1, 1, '2021-03-16 16:07:32');
INSERT INTO `commodity` VALUES (36, 0x3130303336, '新款黑色潮牌牛仔外套', 0, '南极人2021春秋新款黑色潮牌牛仔外套男宽松百搭潮流厚款休闲夹克男外套加肥加大码牛仔衣男 潮 黑色 XL', 6, 100, 168.00, 151.20, 't_007.jpg', 0, 1, 1, 1, '2021-03-16 16:23:08');
INSERT INTO `commodity` VALUES (37, 0x3130303337, '夏季棉质黑色短袖t恤', 0, '南极人夏季棉质黑色短袖t恤男宽松潮牌ins印花半袖大码胖子打底衫潮流体恤 黑色 XL', 7, 100, 89.00, 87.00, 't_008.jpg', 0, 1, 1, 1, '2021-03-16 18:18:55');
INSERT INTO `commodity` VALUES (38, 0x3130303338, '纯色棉质短袖t恤', 0, '南极人夏季潮牌纯色棉质短袖t恤男宽松潮流圆领胖子半袖大码男生T恤男装上衣潮 米黄色 L', 7, 100, 98.00, 90.00, 't_009.jpg', 0, 1, 1, 1, '2021-03-16 16:23:12');
INSERT INTO `commodity` VALUES (39, 0x3130303339, '新款休闲工装裤', 0, '南极人2021春夏季新款休闲工装裤男潮牌宽松束脚裤男哈伦运动韩版多口袋潮流修身长裤子男装 浅绿色 38', 7, 100, 168.00, 166.00, 't_010.jpg', 0, 1, 1, 1, '2021-03-16 16:23:37');
INSERT INTO `commodity` VALUES (40, 0x3130303430, 'Matri Mr牛仔夹克', 1, 'Matri Mr牛仔夹克男秋季新款宽松破洞牛仔外套男潮流韩版工装夹克百搭港风ins衣服 浅蓝色 L', 6, 100, 95.00, NULL, 't_011.jpg', 0, 1, 1, 1, '2021-03-16 16:23:45');
INSERT INTO `commodity` VALUES (41, 0x3130303431, '博谦 新款牛仔外套', 1, '博谦 2021春秋季新款牛仔外套男士夹克大码韩版休闲棉料学生潮流上衣修身青年刺绣男装bf风多口袋 TBK 819 M', 6, 100, 198.00, 168.00, 't_012.jpg', 0, 1, 1, 1, '2021-03-16 18:17:47');
INSERT INTO `commodity` VALUES (42, 0x3130303432, '男士春秋薄款茄克衫', 1, '富贵鸟外套男夹克男士春天秋季薄款茄克衫加绒加厚棉服中年爸爸装休闲大码男装立领棒球服棉袄衣服褂子 军绿薄款 XL(130-145斤)', 6, 100, 168.00, NULL, 't_013.jpg', 0, 1, 1, 1, '2021-03-16 16:23:53');
INSERT INTO `commodity` VALUES (43, 0x3130303433, '花花公子休闲夹克', 1, '花花公子休闲夹克男外套2021春季新款男士韩版潮牌多口袋立领夹克衫男装时尚潮流修身棒球服 2021黑色', 6, 100, 398.00, 269.00, 't_014.jpg', 0, 1, 1, 1, '2021-03-16 16:23:54');
INSERT INTO `commodity` VALUES (44, 0x3130303434, 'CROCODILE 夹克男', 1, '鳄鱼恤 CROCODILE 夹克男春秋新潮流休闲修身连帽外套男韩版帅气风衣男时尚运动男装 YF 1208 黑色 XL', 6, 100, 128.00, 121.00, 't_015.jpg', 0, 1, 1, 1, '2021-03-16 16:23:56');
INSERT INTO `commodity` VALUES (45, 0x3130303435, '花花公子皮衣男', 0, '花花公子（PLAYBOY）皮衣男2021春季韩版夹克男款休闲棒球领外套男士帅气机车皮衣男装 黑色', 8, 100, 298.00, 292.00, 't_016.jpg', 0, 1, 1, 1, '2021-03-17 16:51:01');
INSERT INTO `commodity` VALUES (46, 0x3130303436, '男士短袖套装T恤', 1, '花花公子旗舰夏季套装男潮流男装青少年宽松个性棉男士短袖T恤T 白灰', 7, 100, 299.00, 139.00, 't_017.jpg', 0, 1, 1, 1, '2021-03-16 16:24:05');
INSERT INTO `commodity` VALUES (47, 0x3130303437, '纯棉印花男士短袖t恤', 1, 'Soinku潮型库纯棉印花男士短袖t恤男圆领2020夏季潮流韩版学生五分袖体恤上衣潮牌半袖T恤衫 8328 白色', 7, 100, 68.00, 44.00, 't_018.jpg', 0, 1, 1, 1, '2021-03-16 16:24:11');
INSERT INTO `commodity` VALUES (48, 0x3130303438, '男士夏装短袖T恤', 1, '花花公子旗舰短袖T恤男2020新款夏季假两件男装衣服纯棉男士夏装 827-56黑色', 7, 100, 188.00, 90.00, 't_019.jpg', 0, 1, 1, 1, '2021-03-16 16:24:13');
INSERT INTO `commodity` VALUES (49, 0x3130303439, '套装男短袖T恤', 1, '花花公子旗舰套装男短袖T恤夏季潮流男士潮牌夏装学生青少年男装T 824-12深灰', 7, 100, 269.00, 169.00, 't_020.jpg', 0, 1, 1, 1, '2021-03-16 16:24:14');
INSERT INTO `commodity` VALUES (50, 0x3130303530, '卫衣男秋冬季外套', 1, '【两件装】南极人卫衣男2020秋冬季外套男新品加绒连帽打底衫男士韩版潮流长袖T恤一套运动卫衣套装男装 2061黑卡加绒款(卫衣+裤子)', 5, 100, 298.00, 121.00, 't_021.jpg', 0, 1, 1, 1, '2021-03-16 16:24:24');
INSERT INTO `commodity` VALUES (51, 0x3130303531, '潮型库连帽卫衣', 1, 'Soinku潮型库连帽卫衣男生韩版潮流学生帅气ins潮牌印花渐变休闲上衣服外套男装 6633卡灰色', 5, 10, 138.00, 91.00, 't_022.jpg', 0, 1, 1, 1, '2021-03-16 16:24:25');
INSERT INTO `commodity` VALUES (52, 0x3130303532, '春新款纯色棉质卫衣', 1, '安踏卫衣男装2021春新款纯色棉质舒适保暖上衣休闲运动套头衫长袖T恤内搭秋衣', 5, 100, 134.00, 114.00, 't_023.jpg', 0, 1, 1, 1, '2021-03-16 16:24:27');
INSERT INTO `commodity` VALUES (53, 0x3130303533, '太子龙长袖T恤', 1, '太子龙 TEDELON 长袖T恤男春季男装衣服圆领个性潮流卫衣男印花图案修身休闲舒适打底上衣MYY5102 白色', 7, 100, 99.00, 94.00, 't_024.jpg', 0, 1, 1, 1, '2021-03-16 16:24:29');
INSERT INTO `commodity` VALUES (54, 0x3130303534, '卫衣男无帽圆领', 1, '花花公子卫衣男无帽圆领假两件新款青少年打底衫学生外穿长袖T恤衫 白色', 5, 100, 269.00, 139.00, 't_025.jpg', 0, 1, 1, 1, '2021-03-16 16:07:33');
INSERT INTO `commodity` VALUES (55, 0x3130303535, '吉普卫衣男春秋新款', 1, 'JEEP吉普卫衣男2021春秋新款男士长袖圆领T恤宽松大码纯色运动休闲时尚棉质中青年学生打底衫秋衣服 6527(黑色)', 5, 100, 134.00, 88.00, 't_026.jpg', 0, 1, 1, 1, '2021-03-16 16:24:33');
INSERT INTO `commodity` VALUES (56, 0x3130303536, '男山羊皮夹克', 1, '意大利风格品牌春季新款海宁真皮皮衣男山羊皮夹克外套特大码商务休闲中年男士爸爸装加棉正装 黑色单皮', 8, 100, 878.00, 800.00, 't_027.jpg', 0, 1, 1, 1, '2021-03-16 16:24:39');
INSERT INTO `commodity` VALUES (57, 0x3130303537, '恒源祥牛皮皮衣', 1, '恒源祥高档品牌男装头层牛皮皮衣男真皮男士外套中年休闲直筒商务西装领春季 典雅黑色', 8, 100, 12999.00, 12349.05, 't_028.jpg', 0, 1, 1, 1, '2021-03-16 16:24:40');
INSERT INTO `commodity` VALUES (58, 0x3130303538, '海宁皮革上衣 ', 1, ' 恒源祥真皮皮衣男士立领纯色休闲保暖潮流男装海宁皮革上衣 15905581 黑色', 8, 100, 1280.00, 1234.05, 't_029.jpg', 0, 1, 1, 1, '2021-03-16 16:24:41');
INSERT INTO `commodity` VALUES (59, 0x3130303539, '太子龙牛仔夹克男', 1, '太子龙 TEDELON 夹克男立领纯色工装外套防风衣服男潮流休闲上衣时尚百搭夹克衫运动男装JQN J118 黑色 XL', 6, 100, 148.00, 141.00, 't_001.jpg', 0, 1, 1, 1, '2021-03-16 16:25:17');
INSERT INTO `commodity` VALUES (60, 0x3130303630, '太子龙牛仔裤', 1, '太子龙（TEDELON）牛仔裤男裤子男工装裤时尚修身小脚男装系带束脚弹力潮男士休闲裤 MXK802 灰色 XL', 6, 100, 148.00, 138.00, 't_002.jpg', 0, 1, 1, 1, '2021-03-16 16:25:17');
INSERT INTO `commodity` VALUES (61, 0x3130303631, '太子龙短袖T恤', 1, '太子龙 TEDELON 短袖T恤男夏季男装衣服圆领个性潮流印花图案修身休闲舒适打底衫MYY2102 深灰 XL', 7, 100, 99.00, 93.00, 't_003.jpg', 0, 1, 1, 1, '2021-03-16 16:25:17');
INSERT INTO `commodity` VALUES (62, 0x3130303632, '太子龙卫衣男', 1, '太子龙 TEDELON 卫衣男春秋季长袖T恤圆领拼色套头男士上衣时尚休闲潮流男装外套 YPY3096 灰色 XL', 5, 100, 138.00, 128.00, 't_004.jpg', 0, 1, 1, 1, '2021-03-16 16:25:18');
INSERT INTO `commodity` VALUES (63, 0x3130303633, '太子龙长袖衬衫男', 0, '太子龙 TEDELON 长袖衬衫男春季格子衫男士潮流时尚休闲上衣潮流条纹衬衫外套男装 KXP G37 深灰 XL', 5, 100, 138.00, 131.00, 't_005.jpg', 0, 1, 1, 1, '2021-03-16 16:25:18');
INSERT INTO `commodity` VALUES (64, 0x3130303634, '春秋季帅气黑色牛仔外套', 0, '南极人2021春秋季帅气黑色牛仔外套男韩版修身百搭夹克外套大码学生上衣服潮流 916-亮黑 XL', 6, 100, 158.00, NULL, 't_006.jpg', 0, 1, 1, 1, '2021-03-16 18:12:44');
INSERT INTO `commodity` VALUES (65, 0x3130303635, '新款黑色潮牌牛仔外套', 1, '南极人2021春秋新款黑色潮牌牛仔外套男宽松百搭潮流厚款休闲夹克男外套加肥加大码牛仔衣男 潮 黑色 XL', 6, 100, 168.00, 151.20, 't_007.jpg', 0, 1, 1, 1, '2021-03-16 16:07:34');
INSERT INTO `commodity` VALUES (66, 0x3130303636, '夏季棉质黑色短袖t恤', 1, '南极人夏季棉质黑色短袖t恤男宽松潮牌ins印花半袖大码胖子打底衫潮流体恤 黑色 XL', 7, 100, 89.00, 80.00, 't_008.jpg', 0, 1, 1, 1, '2021-03-16 18:19:16');
INSERT INTO `commodity` VALUES (67, 0x3130303637, '纯色棉质短袖t恤', 0, '南极人夏季潮牌纯色棉质短袖t恤男宽松潮流圆领胖子半袖大码男生T恤男装上衣潮 米黄色 L', 7, 100, 98.00, 90.00, 't_009.jpg', 0, 1, 1, 1, '2021-03-16 16:25:18');
INSERT INTO `commodity` VALUES (68, 0x3130303638, '新款休闲工装裤', 0, '南极人2021春夏季新款休闲工装裤男潮牌宽松束脚裤男哈伦运动韩版多口袋潮流修身长裤子男装 浅绿色 38', 7, 100, 168.00, 166.00, 't_010.jpg', 0, 1, 1, 1, '2021-03-16 16:25:18');
INSERT INTO `commodity` VALUES (69, 0x3130303639, 'Matri Mr牛仔夹克', 1, 'Matri Mr牛仔夹克男秋季新款宽松破洞牛仔外套男潮流韩版工装夹克百搭港风ins衣服 浅蓝色 L', 6, 100, 95.00, NULL, 't_011.jpg', 0, 1, 1, 1, '2021-03-16 16:25:18');
INSERT INTO `commodity` VALUES (70, 0x3130303730, '博谦 新款牛仔外套', 1, '博谦 2021春秋季新款牛仔外套男士夹克大码韩版休闲棉料学生潮流上衣修身青年刺绣男装bf风多口袋 TBK 819 M', 6, 100, 198.00, 158.00, 't_012.jpg', 0, 1, 1, 1, '2021-03-16 16:25:18');
INSERT INTO `commodity` VALUES (71, 0x3130303731, '男士春秋薄款茄克衫', 1, '富贵鸟外套男夹克男士春天秋季薄款茄克衫加绒加厚棉服中年爸爸装休闲大码男装立领棒球服棉袄衣服褂子 军绿薄款 XL(130-145斤)', 6, 100, 168.00, NULL, 't_013.jpg', 0, 1, 1, 1, '2021-03-16 16:25:18');
INSERT INTO `commodity` VALUES (72, 0x3130303732, '花花公子休闲夹克', 1, '花花公子休闲夹克男外套2021春季新款男士韩版潮牌多口袋立领夹克衫男装时尚潮流修身棒球服 2021黑色', 6, 100, 398.00, 269.00, 't_014.jpg', 0, 1, 1, 1, '2021-03-16 16:25:18');
INSERT INTO `commodity` VALUES (73, 0x3130303733, 'CROCODILE 夹克男', 0, '鳄鱼恤 CROCODILE 夹克男春秋新潮流休闲修身连帽外套男韩版帅气风衣男时尚运动男装 YF 1208 黑色 XL', 6, 100, 128.00, 121.00, 't_015.jpg', 0, 1, 1, 1, '2021-03-16 16:25:18');
INSERT INTO `commodity` VALUES (74, 0x3130303734, '花花公子皮衣男', 1, '花花公子（PLAYBOY）皮衣男2021春季韩版夹克男款休闲棒球领外套男士帅气机车皮衣男装 黑色', 8, 100, 298.00, 292.00, 't_016.jpg', 0, 1, 1, 1, '2021-03-16 16:25:19');
INSERT INTO `commodity` VALUES (75, 0x3130303735, '男士短袖套装T恤', 1, '花花公子旗舰夏季套装男潮流男装青少年宽松个性棉男士短袖T恤T 白灰', 7, 100, 299.00, 139.00, 't_017.jpg', 0, 1, 1, 1, '2021-03-16 16:25:19');
INSERT INTO `commodity` VALUES (76, 0x3130303736, '纯棉印花男士短袖t恤', 1, 'Soinku潮型库纯棉印花男士短袖t恤男圆领2020夏季潮流韩版学生五分袖体恤上衣潮牌半袖T恤衫 8328 白色', 7, 100, 68.00, 44.00, 't_018.jpg', 0, 1, 1, 1, '2021-03-16 16:25:19');
INSERT INTO `commodity` VALUES (77, 0x3130303737, '男士夏装短袖T恤', 1, '花花公子旗舰短袖T恤男2020新款夏季假两件男装衣服纯棉男士夏装 827-56黑色', 7, 100, 188.00, 90.00, 't_019.jpg', 0, 1, 1, 1, '2021-03-16 16:25:19');
INSERT INTO `commodity` VALUES (78, 0x3130303738, '套装男短袖T恤', 1, '花花公子旗舰套装男短袖T恤夏季潮流男士潮牌夏装学生青少年男装T 824-12深灰', 7, 100, 269.00, 169.00, 't_020.jpg', 0, 1, 1, 1, '2021-03-16 16:25:19');
INSERT INTO `commodity` VALUES (79, 0x3130303739, '卫衣男秋冬季外套', 1, '【两件装】南极人卫衣男2020秋冬季外套男新品加绒连帽打底衫男士韩版潮流长袖T恤一套运动卫衣套装男装 2061黑卡加绒款(卫衣+裤子)', 5, 100, 298.00, 121.00, 't_021.jpg', 0, 1, 1, 1, '2021-03-16 16:25:19');
INSERT INTO `commodity` VALUES (80, 0x3130303830, '潮型库连帽卫衣', 1, 'Soinku潮型库连帽卫衣男生韩版潮流学生帅气ins潮牌印花渐变休闲上衣服外套男装 6633卡灰色', 5, 100, 138.00, 91.00, 't_022.jpg', 0, 1, 1, 1, '2021-03-16 16:25:19');
INSERT INTO `commodity` VALUES (81, 0x3130303831, '春新款纯色棉质卫衣', 1, '安踏卫衣男装2021春新款纯色棉质舒适保暖上衣休闲运动套头衫长袖T恤内搭秋衣', 5, 100, 134.00, 114.00, 't_023.jpg', 0, 1, 1, 1, '2021-03-16 16:25:19');
INSERT INTO `commodity` VALUES (82, 0x3130303832, '太子龙长袖T恤', 1, '太子龙 TEDELON 长袖T恤男春季男装衣服圆领个性潮流卫衣男印花图案修身休闲舒适打底上衣MYY5102 白色', 7, 100, 99.00, 94.00, 't_024.jpg', 0, 1, 1, 1, '2021-03-16 16:25:19');
INSERT INTO `commodity` VALUES (83, 0x3130303833, '卫衣男无帽圆领', 1, '花花公子卫衣男无帽圆领假两件新款青少年打底衫学生外穿长袖T恤衫 白色', 5, 100, 269.00, 139.00, 't_025.jpg', 0, 1, 1, 1, '2021-03-16 16:25:19');
INSERT INTO `commodity` VALUES (84, 0x3130303834, '吉普卫衣男春秋新款', 1, 'JEEP吉普卫衣男2021春秋新款男士长袖圆领T恤宽松大码纯色运动休闲时尚棉质中青年学生打底衫秋衣服 6527(黑色)', 5, 100, 134.00, 88.00, 't_026.jpg', 0, 1, 1, 1, '2021-03-16 16:07:35');
INSERT INTO `commodity` VALUES (85, 0x3130303835, '男山羊皮夹克', 1, '意大利风格品牌春季新款海宁真皮皮衣男山羊皮夹克外套特大码商务休闲中年男士爸爸装加棉正装 黑色单皮', 6, 100, 878.00, 800.00, 't_027.jpg', 0, 1, 1, 1, '2021-03-16 16:07:35');

-- ----------------------------
-- Table structure for commodity_brand
-- ----------------------------
DROP TABLE IF EXISTS `commodity_brand`;
CREATE TABLE `commodity_brand`  (
  `brand_id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`brand_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of commodity_brand
-- ----------------------------
INSERT INTO `commodity_brand` VALUES (1, '国际品牌');
INSERT INTO `commodity_brand` VALUES (2, '中国制造');

-- ----------------------------
-- Table structure for commodity_category
-- ----------------------------
DROP TABLE IF EXISTS `commodity_category`;
CREATE TABLE `commodity_category`  (
  `category` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`category`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of commodity_category
-- ----------------------------
INSERT INTO `commodity_category` VALUES (1, '套装');
INSERT INTO `commodity_category` VALUES (2, '春秋款');
INSERT INTO `commodity_category` VALUES (3, '秋冬款');
INSERT INTO `commodity_category` VALUES (4, '夏季款');
INSERT INTO `commodity_category` VALUES (5, '卫衣/衬衫');
INSERT INTO `commodity_category` VALUES (6, '夹克/外套');
INSERT INTO `commodity_category` VALUES (7, 'T恤/裤子');
INSERT INTO `commodity_category` VALUES (8, '皮革');
INSERT INTO `commodity_category` VALUES (9, '童装');

-- ----------------------------
-- Table structure for commodity_color
-- ----------------------------
DROP TABLE IF EXISTS `commodity_color`;
CREATE TABLE `commodity_color`  (
  `color_id` int(11) NOT NULL AUTO_INCREMENT,
  `color_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`color_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of commodity_color
-- ----------------------------
INSERT INTO `commodity_color` VALUES (1, '酒红色');
INSERT INTO `commodity_color` VALUES (2, '米白色');
INSERT INTO `commodity_color` VALUES (3, '渐变色');
INSERT INTO `commodity_color` VALUES (4, '淡蓝色');
INSERT INTO `commodity_color` VALUES (5, '炫黑色');

-- ----------------------------
-- Table structure for commodity_crowd
-- ----------------------------
DROP TABLE IF EXISTS `commodity_crowd`;
CREATE TABLE `commodity_crowd`  (
  `crowd_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '适应人群id',
  `crowd_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '人群',
  PRIMARY KEY (`crowd_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of commodity_crowd
-- ----------------------------
INSERT INTO `commodity_crowd` VALUES (1, '婴儿');
INSERT INTO `commodity_crowd` VALUES (2, '学生');
INSERT INTO `commodity_crowd` VALUES (3, '青少年');
INSERT INTO `commodity_crowd` VALUES (4, '中年');
INSERT INTO `commodity_crowd` VALUES (5, '老年');
INSERT INTO `commodity_crowd` VALUES (6, '病人');
INSERT INTO `commodity_crowd` VALUES (7, '孕妇');
INSERT INTO `commodity_crowd` VALUES (8, '儿童');

-- ----------------------------
-- Table structure for commodity_material
-- ----------------------------
DROP TABLE IF EXISTS `commodity_material`;
CREATE TABLE `commodity_material`  (
  `material_id` int(11) NOT NULL AUTO_INCREMENT,
  `material_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`material_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of commodity_material
-- ----------------------------
INSERT INTO `commodity_material` VALUES (1, '水貂');
INSERT INTO `commodity_material` VALUES (2, '真皮');
INSERT INTO `commodity_material` VALUES (3, '棉料');
INSERT INTO `commodity_material` VALUES (4, '轻薄');
INSERT INTO `commodity_material` VALUES (5, '纤维');

-- ----------------------------
-- Table structure for commodity_size
-- ----------------------------
DROP TABLE IF EXISTS `commodity_size`;
CREATE TABLE `commodity_size`  (
  `size_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '商品 码数 id',
  `size_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '码数 名',
  PRIMARY KEY (`size_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of commodity_size
-- ----------------------------
INSERT INTO `commodity_size` VALUES (1, 'S 100斤以内');
INSERT INTO `commodity_size` VALUES (2, 'M 100-120斤');
INSERT INTO `commodity_size` VALUES (3, 'L 120-130斤');
INSERT INTO `commodity_size` VALUES (4, 'XL 130-145斤');
INSERT INTO `commodity_size` VALUES (5, '2XL 145-160斤');
INSERT INTO `commodity_size` VALUES (6, '3XL 160-180斤');
INSERT INTO `commodity_size` VALUES (7, '4XL 180-200斤');
INSERT INTO `commodity_size` VALUES (8, '[%定制%]');

-- ----------------------------
-- Table structure for manager
-- ----------------------------
DROP TABLE IF EXISTS `manager`;
CREATE TABLE `manager`  (
  `mid` int(11) NOT NULL AUTO_INCREMENT COMMENT '管理员ID',
  `mcode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '管理员编号',
  `mpwd` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `mname` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `mage` int(2) NULL DEFAULT NULL COMMENT '性别',
  `mphone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电话',
  `maddress` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地址',
  `verify` int(2) NULL DEFAULT 0 COMMENT '是否被核实[1,是, 0,否]',
  `position` int(2) NULL DEFAULT NULL COMMENT '职务',
  `mauthority` int(10) NULL DEFAULT NULL COMMENT '员工权限',
  PRIMARY KEY (`mid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of manager
-- ----------------------------
INSERT INTO `manager` VALUES (1, 'admin', 'admin', '小胖', 20, '19831831996', '北京朝阳区', 1, 1, 1);

-- ----------------------------
-- Table structure for manager_position
-- ----------------------------
DROP TABLE IF EXISTS `manager_position`;
CREATE TABLE `manager_position`  (
  `p_id` int(2) NOT NULL AUTO_INCREMENT,
  `p_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`p_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of manager_position
-- ----------------------------
INSERT INTO `manager_position` VALUES (1, '超级管理员');
INSERT INTO `manager_position` VALUES (2, '订单管理员');
INSERT INTO `manager_position` VALUES (3, '商品管理员');
INSERT INTO `manager_position` VALUES (4, '类型管理员');
INSERT INTO `manager_position` VALUES (5, '用户管理员');

-- ----------------------------
-- Table structure for manager_proxy
-- ----------------------------
DROP TABLE IF EXISTS `manager_proxy`;
CREATE TABLE `manager_proxy`  (
  `proxy_id` int(11) NOT NULL AUTO_INCREMENT,
  `proxy_Scope` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`proxy_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of manager_proxy
-- ----------------------------
INSERT INTO `manager_proxy` VALUES (1, '1');
INSERT INTO `manager_proxy` VALUES (2, '2');
INSERT INTO `manager_proxy` VALUES (3, '3');
INSERT INTO `manager_proxy` VALUES (4, '4');
INSERT INTO `manager_proxy` VALUES (5, '5');

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order`  (
  `oid` int(11) NOT NULL AUTO_INCREMENT COMMENT '订单ID √',
  `orderCode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单号√',
  `xiadantime` datetime(0) NULL DEFAULT NULL COMMENT '下单时间√',
  `fahuotime` datetime(0) NULL DEFAULT NULL COMMENT '发货时间√',
  `ostatus` int(2) NULL DEFAULT NULL COMMENT '订单状态√',
  `oamount` float(10, 2) NULL DEFAULT NULL COMMENT '订单总金额√',
  `o_uid` int(10) NULL DEFAULT NULL COMMENT '属于某用户√',
  PRIMARY KEY (`oid`) USING BTREE,
  INDEX `o_uid`(`o_uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order
-- ----------------------------
INSERT INTO `order` VALUES (1, 'Dh-2021311101029', '2021-03-10 10:11:09', '2021-03-12 08:47:10', 3, 10.00, 1);
INSERT INTO `order` VALUES (2, 'Dh-2021311101030', '2021-02-01 10:11:09', '2021-03-15 02:03:35', 3, 10.00, 1);
INSERT INTO `order` VALUES (3, 'Dh-2021311101032', '2021-03-10 10:11:09', '2021-03-11 10:11:32', 3, 10.00, 1);
INSERT INTO `order` VALUES (4, 'Dh-2021311101033', '2021-03-03 09:11:09', '2021-03-11 09:11:32', 3, 10.00, 1);
INSERT INTO `order` VALUES (5, 'Dh-2021311101034', '2021-02-02 10:11:09', '2021-03-11 10:11:32', 3, 10.00, 1);
INSERT INTO `order` VALUES (6, 'Dh-2021311101035', '2021-02-03 09:11:09', '2021-03-11 09:11:32', 3, 10.00, 1);
INSERT INTO `order` VALUES (7, 'Dh-2021311101036', '2021-02-04 10:11:09', '2021-03-11 10:11:32', 3, 10.00, 1);
INSERT INTO `order` VALUES (8, 'Dh-2021311101037', '2021-02-04 10:11:09', '2021-03-11 10:11:32', 3, 10.00, 1);
INSERT INTO `order` VALUES (9, 'Dh-2021311101038', '2021-03-05 10:11:09', '2021-03-11 10:11:32', 3, 10.00, 1);
INSERT INTO `order` VALUES (10, 'Dh-2021311101039', '2021-03-06 10:11:09', '2021-03-11 10:11:32', 3, 10.00, 1);
INSERT INTO `order` VALUES (11, 'Dh-2021311101040', '2021-03-07 09:11:09', '2021-03-11 09:11:32', 3, 10.00, 1);
INSERT INTO `order` VALUES (12, 'Dh-2021311101041', '2021-03-08 10:11:09', '2021-03-11 10:11:32', 3, 10.00, 1);
INSERT INTO `order` VALUES (13, 'Dh-2021311101042', '2021-03-09 10:11:09', '2021-03-11 10:11:32', 3, 10.00, 1);
INSERT INTO `order` VALUES (14, 'Dh-2021311101043', '2021-03-10 09:11:09', '2021-03-11 09:11:32', 3, 10.00, 1);
INSERT INTO `order` VALUES (15, 'Dh-2021311101044', '2021-03-11 10:11:09', '2021-03-12 08:40:16', 3, 10.00, 1);
INSERT INTO `order` VALUES (16, 'Dh-2021311101045', '2021-03-12 09:11:09', '2021-03-11 09:11:32', 3, 10.00, 1);
INSERT INTO `order` VALUES (17, 'Dh-2021311101046', '2021-03-10 10:11:09', '2021-03-11 10:11:32', 3, 10.00, 1);
INSERT INTO `order` VALUES (18, 'Dh-2021311101029', '2021-02-01 10:11:09', '2021-03-11 10:11:32', 3, 10.00, 1);
INSERT INTO `order` VALUES (19, 'Dh-2021311101034', '2021-03-10 10:11:09', '2021-03-11 09:11:32', 3, 10.00, 1);
INSERT INTO `order` VALUES (20, 'Dh-2021311101029', '2021-03-03 09:11:09', '2021-03-11 10:11:32', 3, 10.00, 1);
INSERT INTO `order` VALUES (21, 'Dh-2021311101029', '2021-02-02 10:11:09', '2021-03-11 10:11:32', 3, 10.00, 1);
INSERT INTO `order` VALUES (22, 'Dh-2021311101029', '2021-02-03 09:11:09', '2021-03-11 10:11:32', 3, 10.00, 1);
INSERT INTO `order` VALUES (23, 'Dh-2021311101029', '2021-02-04 10:11:09', '2021-03-11 10:11:32', 3, 10.00, 1);
INSERT INTO `order` VALUES (24, 'Dh-2021311101034', '2021-02-04 10:11:09', '2021-03-11 09:11:32', 3, 10.00, 1);
INSERT INTO `order` VALUES (25, 'Dh-2021311101029', '2021-03-05 10:11:09', '2021-03-11 10:11:32', 3, 10.00, 1);
INSERT INTO `order` VALUES (26, 'Dh-2021311101034', '2021-03-06 10:11:09', '2021-03-11 09:11:32', 3, 10.00, 1);
INSERT INTO `order` VALUES (27, 'Dh-2021311101029', '2021-03-07 09:11:09', '2021-03-11 10:11:32', 3, 10.00, 1);

-- ----------------------------
-- Table structure for order_item
-- ----------------------------
DROP TABLE IF EXISTS `order_item`;
CREATE TABLE `order_item`  (
  `order_id_xq` int(11) NOT NULL COMMENT '订单id ',
  `commodity_id_xq` int(11) NOT NULL COMMENT '商品id',
  `order_num` int(11) NOT NULL COMMENT '购买数量'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_item
-- ----------------------------
INSERT INTO `order_item` VALUES (1, 1, 2);
INSERT INTO `order_item` VALUES (1, 2, 2);
INSERT INTO `order_item` VALUES (1, 3, 1);
INSERT INTO `order_item` VALUES (12, 4, 3);
INSERT INTO `order_item` VALUES (12, 5, 1);
INSERT INTO `order_item` VALUES (15, 6, 1);
INSERT INTO `order_item` VALUES (2, 7, 2);
INSERT INTO `order_item` VALUES (2, 8, 1);
INSERT INTO `order_item` VALUES (3, 9, 1);
INSERT INTO `order_item` VALUES (4, 10, 2);
INSERT INTO `order_item` VALUES (2, 11, 3);
INSERT INTO `order_item` VALUES (3, 12, 2);
INSERT INTO `order_item` VALUES (5, 13, 2);
INSERT INTO `order_item` VALUES (5, 3, 1);

-- ----------------------------
-- Table structure for order_status
-- ----------------------------
DROP TABLE IF EXISTS `order_status`;
CREATE TABLE `order_status`  (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `sname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_status
-- ----------------------------
INSERT INTO `order_status` VALUES (1, '代付款');
INSERT INTO `order_status` VALUES (2, '已付款,代发货');
INSERT INTO `order_status` VALUES (3, '已发货,代签收');
INSERT INTO `order_status` VALUES (4, '已完成');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `uid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `usercode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户编码',
  `pwd` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户密码',
  `gender` int(255) NULL DEFAULT NULL COMMENT '性别',
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '真实姓名',
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地址',
  `createdBy` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `creationDate` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modifyBy` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `modifyDate` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `u_rid` int(11) NULL DEFAULT NULL COMMENT '用户下收货地址',
  `u_oid` int(11) NULL DEFAULT NULL COMMENT '用户下的订单',
  PRIMARY KEY (`uid`) USING BTREE,
  INDEX `u_rid`(`u_rid`) USING BTREE,
  INDEX `u_oid`(`u_oid`) USING BTREE,
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`u_rid`) REFERENCES `user_receipt` (`r_uid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_ibfk_2` FOREIGN KEY (`u_oid`) REFERENCES `order` (`o_uid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'admin', 'admin', 1, '小胖', '19831831996', '123@163.com', '北京市东城区景山前街4号', '', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` VALUES (3, 'admin2', '123123', 1, '皇子', '15130049334', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` VALUES (4, 'user', '123123', 2, '盖伦', '15130049334', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` VALUES (5, 'user1', '123123', 2, '赵信', '15130049334', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for user_receipt
-- ----------------------------
DROP TABLE IF EXISTS `user_receipt`;
CREATE TABLE `user_receipt`  (
  `rid` int(11) NOT NULL AUTO_INCREMENT COMMENT '收货ID',
  `rname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '收货人姓名',
  `rphone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '手机号',
  `raddress` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '地址',
  `r_uid` int(255) NOT NULL COMMENT '属于某用户',
  PRIMARY KEY (`rid`) USING BTREE,
  INDEX `r_uid`(`r_uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_receipt
-- ----------------------------
INSERT INTO `user_receipt` VALUES (1, '小胖', '19831831996', '北京市东城区景山前街4号', 1);
INSERT INTO `user_receipt` VALUES (2, '小胖', '17631348487', '北京市东城区东长安街16号', 1);
INSERT INTO `user_receipt` VALUES (3, '张三', '1233211231', '北京市东城区朝内南小街', 2);
INSERT INTO `user_receipt` VALUES (4, '小猪', '1231233211', '北京市东城区天坛东里甲1号', 2);

SET FOREIGN_KEY_CHECKS = 1;
