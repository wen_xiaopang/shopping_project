function jump_to(num){
    // alert(num);
    // alert(frm);
    //验证用户的输入
    var regexp=/^[1-9]\d*$/;
    var totalPageCount = document.getElementById("totalPageCount").value;
    // alert(totalPageCount);
    if(!regexp.test(num)){
        alert("请输入大于0的正整数！");
        return false;
    }else if((num-totalPageCount) > 0){
        alert("请输入小于总页数的页码");
        return false;
    }else{
        location = "${dq_url}?pageNum="+num+"&cname=${sp_xq}";
    }
}

$(function () {
    $("#cname").blur(function () {
        var username = $("#cname").val();
        //发送异步请求
        $.post("/youce_cxAjax",{cname:username},function (data) {
            if (data.cx_cname=="true"){
                $("#cname").attr("placeholder","请输入商品名")
            }else{
                $("#cname").attr("value",${sp_xq})
            }
        });
    });

    $("#cx_submit").click(function () {
        var leixing =$("#category").val();
        var sptime = $("#sp_time").prop('checked');
        // alert(sptime);
        var spjiage = $("#sp_jiage").prop('checked');
        // alert(spjiage);
        // if (sptime==false && spjiage==false){
        // $("#sp_time").attr("checked","checked"); //默认按照时间查询
        // }
        var paixu = $(":radio:checked").val();
        $.post("/youce_cxqita", {category:leixing,sp_time:sptime,sp_jiage:spjiage,sp_paixu:paixu},
            function (data) {
                if (data.cx_category=="false"){
                    $("#category").attr("value",${sp_lx_id})
                }
                $(":radio").removeAttr('checked');
                if (data.cx_sp_paixu=="1"){
                    $("#sp_shengxu").attr("checked","checked"); //默认选中升序
                }else if (data.cx_sp_paixu=="2"){
                    $("#sp_jiangxu").attr("checked","checked"); //默认选中降序
                }
            })
        // var item = $(":radio:checked");
        // var len=item.length;
        // if(len>0){
        //     alert("yes--选中的值为："+$(":radio:checked").val());
        // }
    });
});