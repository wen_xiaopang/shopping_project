// <!--// 格式： 当前时间：2020年3月17-0时54分14秒 -->
var t = null;
t = setTimeout(time, 1000);//開始运行
function time() {
    clearTimeout(t);//清除定时器
    dt = new Date();
    var y = dt.getFullYear();
    var mt = dt.getMonth() + 1;
    var day = dt.getDate();
    var h = dt.getHours();//获取时
    var m = dt.getMinutes();//获取分
    var s = dt.getSeconds();//获取秒
    var str = "星期" + "日一二三四五六".charAt(new Date().getDay());
    document.querySelector(".this_time").innerHTML = '当前时间: ' + y + "-" + mt + "-" + day +" \n " + h + ":" + m + ":" + s +" "+ str ;
    t = setTimeout(time, 1000); //设定定时器，循环运行
}