$(jQuery).ready(function(){
    //单方面变红
    // $(".sp_ul_1>li").click(function () {
    //     // $(this).css({border:"1px solid red"});
    // })
    // 优化写法。
    var $chima;
    $(".sp_ul_1>li").click(function () {
        // 把上个设置回默认色（白色），避免第一次
        if ($chima)
            $chima.css('border', '1px solid #666');

        $(this).css('border', '1px solid red')

        // 开头把这句话放在if里面，但是这样代码的意思就不清晰
        // 当前的就变成上一个了。
        $chima = $(this);
    });
    var $yanse;
    $(".sp_ul_2>li").click(function () {
        // 把上个设置回默认色（白色），避免第一次
        if ($yanse)
            $yanse.css('border', '1px solid #666');

        $(this).css('border', '1px solid red')

        // 开头把这句话放在if里面，但是这样代码的意思就不清晰
        // 当前的就变成上一个了。
        $yanse = $(this);
    });
});
