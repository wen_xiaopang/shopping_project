<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<title>修改信息</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- 引入 Bootstrap -->
<link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

<script type="text/javascript" src="../../statics/js/jquery-3.3.1.min.js"></script>


<script>

    $(document).ready(function () {
        var isFlag=true;

        //密码判断
        $("#pwd").blur(function () {
            if($("#pwd").val()==null || $("#pwd").val()=="" || $("#pwd").val().length<6 || $("#pwd").val().length>12){
                $("#a2:first").html("密码格式错误").css("color","red");
                isFlag=false;
            }else {
                $("#a2:first").html("√√√").css("color","green");
                isFlag=true;
            }
        })


        //判断手机号格式
        $("#phone").blur(function () {
            phone=$("#phone").val();
            // 以13、4...开头的，后边必须是9位
            if (!(/^1(3|4|5|7|8)\d{9}$/.test(phone))||phone==""){
                $("#a4:first").html("请输入正确的手机号").css("color","red");
                isFlag=false;
            }else {
                $("#a4:first").html("√√√").css("color","green");
                isFlag=true;
            }
        })


        /*表单验证*/
        $("Form").submit(function () {
            return isFlag;
        })


    })

</script>
<style type="text/css">
    .inputbox {
        /*border: 1px red solid;*/
        /*padding: 0 0 0 30px;*/
        margin: 2px 5px;
        height: 50px;
        line-height: 50px;
    }
    .inputbox label{
        font:  18px "楷体";
    }
    .inputbox input,select {
        width: 210px;
        height: 40px;
        padding: 8px 5px 8px 20px;
        margin: 5px;
        border: 1px solid rgb(178, 178, 178);
        border-radius: 3px;
        -webkit-box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.6) inset;
        -moz-box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.6) inset;
        box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.6) inset;
    }
    .subBtn{
        /*margin: 5px auto;*/
        padding: 5px 0 5px 20px;
        width: 160px;
        height: 50px;
        line-height: 50px;
        /*border: 1px red solid;*/
    }
    .subBtn_a{
        /*position: relative;*/
        display: inline-block;
        background: #90EE90;
        border: 1px solid #008000;
        border-radius: 4px;
        padding: 4px 12px;
        overflow: hidden;
        color: black;
        text-decoration: none;
        text-indent: 0;
        line-height: 20px;
    }
    .subBtn_b{
        /*position: relative;*/
        display: inline-block;
        background: #FF6347;
        border: 1px solid #8B0000;
        border-radius: 4px;
        padding: 4px 12px;
        overflow: hidden;
        color: white;
        text-decoration: none;
        text-indent: 0;
        line-height: 20px;
    }
</style>

<body>
<div class="zhuti_header">
    <!--头部-->
    <%@include file="/WEB-INF/jsp/common/head.jsp"%>
</div>
<div class="container" style="position: relative;top: 40px;">
    <div class="row clearfix">
        <div class="col-md-12 column">
            <div class="page-header">
                <h1>
                    <small>修改信息</small>
                </h1>
            </div>
        </div>
    </div>
    <form action="${pageContext.request.contextPath}/updateUser" method="post">
        <input type="hidden" name="uid" value="${user.uid}"/><br><br><br>

        <div class="inputbox">
            <label for="usercode">昵称：</label>
            <input type="text" class="input-text" id="usercode" name="usercode" readonly= "true" value="${user.usercode}" /><span id="a1"></span><br><br><br>
        </div>
        <div class="inputbox">
            <label for="pwd">密码：</label>
            <input type="password" class="input-text" id="pwd" name="pwd" value="${user.pwd}" /><span id="a2"></span><br><br><br>
        </div>
        <div class="inputbox">
            <label for="username">姓名：</label>
            <input type="text" class="input-text" id="username"  name="username"  readonly= "true" value="${user.username}" /><span id="a3"></span><br><br><br>
        </div>
        <div class="inputbox">
            <label for="gender">性别：</label>
            <select name="gender" id="gender">
                <c:if test="${user.gender==1}">
                    <option value="1">------男------</option>
                </c:if>
                <c:if test="${user.gender==2}">
                    <option value="2">------女------</option>
                </c:if>
            </select><br><br><br>
        </div>
        <div class="inputbox">
            <label for="phone">电话：</label>
            <input type="text" class="input-text" id="phone" name="phone"   value="${user.phone}" /><span id="a4"></span><br><br><br>
        </div>
        <div class="subBtn">
            <input type="submit" class="subBtn_a" value="修改"/>
            <input type="reset" class="subBtn_b" value="重置">
        </div>
    </form>
</div>
<!--背景-->
<%@include file="/WEB-INF/jsp/common/foot.jsp"%>