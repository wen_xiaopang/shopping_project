<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>用户管理</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- 引入 Bootstrap -->
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="../../statics/js/jquery-3.3.1.min.js"></script>
</head>


<body>
<div class="zhuti_header">
    <!--头部-->
    <%@include file="/WEB-INF/jsp/common/head.jsp"%>
</div>
<div class="container" style="position: relative;top: 40px;">
    <div class="row clearfix">
        <div class="col-md-12 column">
            <div class="page-header">
                <h1>
                    <small>查看所有用户</small>
                </h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 column">
            <a class="btn btn-primary" href="${pageContext.request.contextPath}/toAddUser">新增</a>
            <a class="btn btn-primary" href="${pageContext.request.contextPath}/allUser1">显示所有用户</a>
        </div>

        <div class="col-md-4 column"></div>

        <div class="col-md-4 column">
            <form action="${pageContext.request.contextPath}/all1" method="post" style="float: right">
                <input type="text" name="usercode" class="form-control" placeholder="请输入要查询的用户编号" >
                <input type="submit" value="查询" class="btn btn-primary">
            </form>

        </div>
    </div>
    <div class="row clearfix">
        <div class="col-md-12 column">
            <table class="table table-hover table-striped">
                <thead>
                <tr>
                    <th>用户ID</th>
                    <th>用户名称</th>
                    <th>用户密码</th>
                    <th>性别</th>
                    <th>真实姓名</th>
                    <th>手机号</th>
<%--                    <th>邮箱</th>--%>
<%--                    <th>地址</th>--%>
<%--                    <th>创建者</th>--%>
<%--                    <th>创建时间</th>--%>
<%--                    <th>更新者</th>--%>
<%--                    <th>更新时间</th>--%>

                </tr>
                </thead>
                <tbody>
                <%--                <c:forEach var="book" items="${requestScope.get('list')}">--%>
                <c:forEach var="user" items="${list}">
                    <tr>
                        <td>${user.uid}</td>
                        <td>${user.usercode}</td>
                        <td>${user.pwd}</td>
                        <td>
                            <c:if test="${user.gender==1}">
                                男
                            </c:if>
                            <c:if test="${user.gender==2}">
                                女
                            </c:if>
                        </td>
                        <td>${user.username}</td>
                        <td>${user.phone}</td>
<%--                        <td>${user.email}</td>--%>
<%--                        <td>${user.address}</td>--%>
<%--                        <td>${user.createdBy}</td>--%>
<%--                        <td>${user.creationDate}</td>--%>
<%--                        <td>${user.modifyBy}</td>--%>
<%--                        <td>${user.modifyDate}</td>--%>

                        <td>
                            <a href="${pageContext.request.contextPath}/toUpdateUser?id=${user.uid}">更改</a> |
                            <a href="${pageContext.request.contextPath}/delUser/${user.uid}">删除</a>
                        </td>

                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!--背景-->
<%@include file="/WEB-INF/jsp/common/foot.jsp"%>