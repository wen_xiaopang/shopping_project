<%--
  Created by IntelliJ IDEA.
  User: dell
  Date: 2020/12/22
  Time: 17:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <title>首页</title>
    <script src="../../statics/js/jquery-3.3.1.min.js"></script>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/statics/css/frame.css" />
    <style type="text/css">
        .xg_yc:hover{
            cursor: pointer;
        }
    </style>
</head>
<body>
<div class="zhuti_header">
    <!--头部-->
    <%@include file="/WEB-INF/jsp/common/head.jsp"%>
</div>
<div class="zhuti" >

    <div class="zhuye"  >
        <!--左侧 类型菜单-->
        <div class="zhuye_leix" >
            <ul class="zhuye_leix_ul">
                <li class="this_time" style="font: normal 16px '宋体'"> 当前时间: 2021-3-20 <br> 8:25:31 星期六</li>
                <li>
                    <a href="/getAllSP">所有商品类型</a>
                </li>
                <c:forEach items="${leixing}" var="spLeiXing">
                    <li id="leix_a${spLeiXing.category}">
                        <a href="${dq_url}?category=${spLeiXing.category}" >
                            <c:if test="${fn:length(spLeiXing.category_name)<7}">
                                <c:out value="${spLeiXing.category_name}"/>
                            </c:if>
                            <c:if test="${fn:length(spLeiXing.category_name) > 6}">
                                <c:out value="${fn:substring(spLeiXing.category_name,0,6)}"/>
                            </c:if>
                        </a>
                    </li>
                </c:forEach>
            </ul>
        </div>

        <div class="zhuye_youce" style="  height: 743px" >
            <!--查询 信息-->
            <div class="zhuye_chax" style=" position: relative;top: 40px;">
                <form action="/shopping_show.html" method="post">
                    <!--隐藏类型-->
                    <c:if test="${sp_lx_id!=null}">
                        <input type="hidden" id="category" name="category" value="${sp_lx_id}"/>
                    </c:if>
                    <div class="chax_inputbox">
                        <label for="cname">商品名：</label>
                        <c:if test="${sp_xq!=null}">
                            <input type="text" class="input-text" id="cname" name="cname" value="${sp_xq}" />
                        </c:if>
                        <c:if test="${sp_xq==null}">
                            <input type="text" class="input-text" id="cname" name="cname" placeholder="请输入商品名"/>
                        </c:if>
                    </div>
                    <div class="chax_inputbox">
                        <c:if test="${sp_cxtime=='true'}">
                            <input type="checkbox" checked id="sp_time" name="sp_time" value="3">
                        </c:if>
                        <c:if test="${sp_cxtime=='false'}">
                            <input type="checkbox" id="sp_time" name="sp_time" value="3">
                        </c:if>
                        <c:if test="${sp_cxtime=='false'} and ${sp_cxjiage=='false'}">
                            <input type="checkbox" checked id="sp_time" name="sp_time" value="3">
                        </c:if>
                        <label for="sp_time"> 时间 </label>
                    </div>
                    <div class="chax_inputbox">
                        <c:if test="${sp_cxjiage=='true'}">
                            <input type="checkbox" checked id="sp_jiage" name="sp_jiage" value="4">
                        </c:if>
                        <c:if test="${sp_cxjiage=='false'}">
                        <input type="checkbox"  id="sp_jiage" name="sp_jiage" value="4">
                        </c:if>
                        <label for="sp_jiage"> 价格</label>
                    </div>
                    <div class="chax_inputbox">
                        <c:if test="${sp_paixu=='1'}">
                            <input type="radio"  id="sp_shengxu" checked class="sp_radio" name="sp_radio" value="1">
                        </c:if>
                        <c:if test="${sp_paixu!='1'}">
                            <input type="radio"  id="sp_shengxu"  class="sp_radio" name="sp_radio" value="1">
                        </c:if>
                        <label for="sp_shengxu"> 升序</label>
                    </div>
                    <div class="chax_inputbox">
                        <c:if test="${sp_paixu=='1'}">
                            <input type="radio"  id="sp_jiangxu" class="sp_radio" name="sp_radio" value="2">
                        </c:if>
                        <c:if test="${sp_paixu!='1'}">
                            <input type="radio"  id="sp_jiangxu" checked class="sp_radio" name="sp_radio" value="2">
                        </c:if>
                        <label for="sp_jiangxu"> 降序</label>
                    </div>
                    <div class="chax_submit">
                        <input type="submit" name="cx_submit" id="cx_submit" value="查询">
                    </div>
                    <c:if test="${adminSession!=null}">
                    <div class="chax_inputbox" id="tj_yc">
                        <span> 添加商品 </span>
                    </div>
                    </c:if>
                </form>
                <!--添加商品的 div-->
                <div class="tianjia_shangpin" id="tj_xs">
                    <h2 >添加商品</h2>
                    <hr >
                    <div class="insert_f">
                        <form id="form_2" action="${pageContext.request.contextPath}/addCommodity?pageNum=${page.pageNum}" method="post">
                            <%--                                <div class="info" align="center">${error }</div>--%>
                            <div class="inputbox">
                                <label for="insert_f_sp_lx">商品类型:</label>
<%--                                <select name="insert_f_sp_lx" id="insert_f_sp_lx">--%>
<%--                                    <c:forEach items="${leixing}" var="lx_name" varStatus="leixing_list">--%>
<%--                                        <option value="${lx_name.category}"><c:out value="${lx_name.category_name}"/></option>--%>
<%--                                    </c:forEach>--%>
<%--                                </select>--%>

                                <input type="text" class="input-text" list="typelist" id="insert_f_sp_lx" name="insert_f_sp_lx" placeholder="请选择已有商品类型" />
                                <datalist id="typelist">
                                    <c:forEach items="${leixing}" var="lx_name" varStatus="leixing_list">
                                        <option value="${lx_name.category}"><c:out value="${lx_name.category_name}"/></option>
                                    </c:forEach>
                                </datalist>
                            </div>
                            <div class="inputbox">
                                <label for="insert_f_sp_mc">商品名称:</label>
                                <input type="text" class="input-text" id="insert_f_sp_mc" name="insert_f_sp_mc" placeholder="商品名称" />
                            </div>
                            <div class="inputbox">
                                <label for="insert_f_sp_jg">商品价格:</label>
                                <input type="text" class="input-text" id="insert_f_sp_jg" name="insert_f_sp_jg" placeholder="商品价格" />
                            </div>
                            <div class="inputbox">
                                <label for="insert_f_sp_kc">商品库存:</label>
                                <input type="text" class="input-text" id="insert_f_sp_kc" name="insert_f_sp_kc" placeholder="商品库存" />
                            </div>
                            <div class="inputbox">
                                <label for="insert_f_sp_fm">商品封面:</label>
                                <input type="file" class="input-text" id="insert_f_sp_fm" name="insert_f_sp_fm" placeholder="商品封面" />
                            </div>
                            <div class="inputbox2">
                                <label for="insert_f_sp_js" class="insert_f_sp_js">商品介绍:</label>
                                <textarea cols="31" rows="5" id="insert_f_sp_js" name="insert_f_sp_js" placeholder="商品介绍"></textarea>
                                <!--            <input type="text" class="input-text" id="insert_f_sp_js" name="mCode" placeholder="商品介绍" />-->
                            </div>
                            <div class="subBtn">
                                <input type="submit" id="subBtn_a_tj" class="subBtn_a" value="提交"/>
                                <input type="button" id="subBtn_b_qx" class="subBtn_b" value="取消"/>
                            </div>
                        </form>
                    </div>
                    <!--  添加商品的 提示-->
                    <script type="text/javascript" >
                        //添加判断
                        jQuery(function ($) {
                            var isflag = false;
                            $("#form_2").submit(function () {
                                let insert_f_sp_lx= $("#insert_f_sp_lx").val();
                                let insert_f_sp_mc = $("#insert_f_sp_mc").val();
                                let insert_f_sp_jg = $("#insert_f_sp_jg").val();
                                let insert_f_sp_kc = $("#insert_f_sp_kc").val();
                                let insert_f_sp_fm = $("#insert_f_sp_fm").val();
                                let insert_f_sp_js = $("#insert_f_sp_js").val();
                                if (isflag==true){
                                    alert("添加成功!");
                                    return isflag;
                                }else
                                if (insert_f_sp_lx==""||insert_f_sp_lx==null){
                                    alert("请选择类型!");
                                    isflag = false;
                                }else
                                if (insert_f_sp_mc==""||insert_f_sp_mc==null){
                                    alert("请输入商品名称!")
                                    isflag = false;
                                }else
                                if (insert_f_sp_jg==""||insert_f_sp_jg==null){
                                    alert("请输入价钱!");
                                    isflag = false;
                                }else
                                if (insert_f_sp_kc==""||insert_f_sp_kc==null){
                                    alert("请输入库存!");
                                    isflag = false;
                                }else
                                if (insert_f_sp_fm==""||insert_f_sp_fm==null){
                                    alert("请选择封面!");
                                    isflag = false;
                                }else
                                if (insert_f_sp_js==""||insert_f_sp_js==null){
                                    alert("请输入 商品介绍内容!");
                                    isflag = false;
                                } else{
                                    isflag = true;
                                    alert("添加成功!");
                                }
                                return isflag;
                            });
                        })

                        $("#tj_yc").click(function() {
                            $("#tj_xs").toggle()
                        })

                        $("#subBtn_b_qx").click(function() {
                            $("#tj_xs").toggle()
                        })
                    </script>
                </div>

            </div>
            <!--右侧 主题显示-->
            <div class="youce_zhuti" style=" position: absolute;top: 80px;">
                <!--图片div-->
                <c:forEach items="${shopping}" var="commodity" varStatus="sp_com_sta">
                        <%--整体 边框--%>
                    <div class="show_tupian">
                    <c:if test="${adminSession!=null}">
                        <%--上下架--%>
                            <div class="tupian_span">
                                <c:if test="${commodity.cstatus==1}">
                                    <span style="background: #B22222;"><a href="/updateSXjia?cstatus=${commodity.cstatus}&cid=${commodity.cid}&pageNum=${page.pageNum}">下架</a></span>
                                </c:if>
                                <c:if test="${commodity.cstatus==0}">
                                    <span style="background: #4169E1;"><a href="/updateSXjia?cstatus=${commodity.cstatus}&cid=${commodity.cid}&pageNum=${page.pageNum}">上架</a></span>
                                </c:if>
                                <span class="xg_yc" id="xg_yc_${commodity.cid}" style="background: #008000; color: #eeeeee;" name="xg_yc" >
                                    修改
                                </span>
                                <span class="xg_yc" id="xg_yc_sc${commodity.cid}" style="background: black; color: #eeeeee;" name="xg_yc" >
                                    删除
                                </span>
                            </div>
                        <!--删除商品的 提示div-->
                        <div class="sc_shangping" style="text-align: center; margin: 0 auto; z-index: 4; box-shadow:4px 4px 5px #999;border-radius:5px; background: #E6E6E6;width: 200px;height: 100px;display: none;position: fixed;top: 240px;left: 650px;"
                             id="yc_sc_div_${commodity.cid}">
                                <p style="font:  bold 20px '楷体';"> 请确定是否删除 <br> 此商品?</p>
                                <div class="subBtn">
                                    <a href='/delShangPing?&cid=${commodity.cid}&pageNum=${page.pageNum}'><input type="button" id="delete_subBtn_s${commodity.cid}" class="subBtn_a" value="是"/></a>
                                    <input type="button"  class="subBtn_b" id="delete_subBtn_${commodity.cid}" value="否"/>
                                </div>
                            <script type="text/javascript" >
                                //添加判断
                                jQuery(function ($) {
                                    var isflag = false;
                                    $("#delete_subBtn_s${commodity.cid}").click(function () {
                                        alert("删除成功!");
                                        isflag=true;
                                        return isflag;
                                    })
                                })

                                //删除_隐藏
                                $("#xg_yc_sc${commodity.cid}").click(function() {
                                    $("#yc_sc_div_${commodity.cid}").toggle();
                                })


                                $("#delete_subBtn_${commodity.cid}").click(function() {
                                    $("#yc_sc_div_${commodity.cid}").toggle()
                                })
                            </script>
                        </div>
                        <!--修改商品的 div-->
                        <div class="tianjia_shangpin" id="xg_xs_${commodity.cid}">
                            <h2 >修改商品</h2>
                            <hr >
                            <div class="insert_f">
                                <form id="from_0${commodity.cid}" action="${pageContext.request.contextPath}/updateComS?cid=${commodity.cid}&pageNum=${page.pageNum}" method="post">
                                        <%--                                <div class="info" align="center">${error }</div>--%>
                                    <div class="inputbox">
                                        <label for="update_f_sp_lx">商品类型:</label>
                                        <input type="text" class="input-text" list="update_typelist" id="update_f_sp_lx" name="update_f_sp_lx" placeholder="当前商品类型:${commodity.category}" />
                                        <datalist id="update_typelist">
                                            <c:forEach items="${leixing}" var="lx_name" varStatus="leixing_list">
                                                <option value="${lx_name.category}"><c:out value="${lx_name.category_name}"/></option>
                                            </c:forEach>
                                        </datalist>
                                    </div>
                                    <div class="inputbox">
                                        <label for="update_f_sp_mc">商品名称:</label>
                                        <input type="text" class="input-text" id="update_f_sp_mc" name="update_f_sp_mc" placeholder="商品名称" value="${commodity.cname}" />
                                    </div>
                                    <div class="inputbox">
                                        <label for="update_f_sp_jg">商品原价:</label>
                                        <input type="text" class="input-text" id="update_f_sp_jg" name="update_f_sp_jg" placeholder="商品原价" value="${commodity.cmoney}"/>
                                    </div>
                                    <div class="inputbox">
                                        <label for="update_f_sp_jg">商品现价:</label>
                                        <input type="text" class="input-text" id="update_f_sp_zhj" name="update_f_sp_zhj" placeholder="商品现价" value="${commodity.discount}"/>
                                    </div>
                                    <div class="inputbox">
                                        <label for="update_f_sp_kc">商品库存:</label>
                                        <input type="text" class="input-text" id="update_f_sp_kc" name="update_f_sp_kc" placeholder="商品库存" value="${commodity.stock}"/>
                                    </div>
                                    <div class="inputbox">
                                        <label for="update_f_sp_fm">商品封面:</label>
                                        <input type="file" class="input-text" id="update_f_sp_fm" name="update_f_sp_fm" placeholder="商品封面" value="${commodity.imgpath}"/>
                                    </div>
                                    <div class="inputbox2">
                                        <label for="update_f_sp_js" class="insert_f_sp_js">商品介绍:</label>
                                        <textarea cols="31" rows="5" id="update_f_sp_js" name="update_f_sp_js" placeholder="${commodity.introduction}" ></textarea>
                                        <!--            <input type="text" class="input-text" id="insert_f_sp_js" name="mCode" placeholder="商品介绍" />-->
                                    </div>
                                    <div class="subBtn">
                                        <input type="submit" class="subBtn_a" value="修改"/>
                                        <input type="button"  class="subBtn_b" id="update_subBtn_${commodity.cid}" value="取消"/>
                                    </div>
                                </form>
                            </div>
                            <script type="text/javascript" >
                                //添加判断
                                jQuery(function ($) {
                                    var isflag = false;
                                    $("#from_0${commodity.cid}").submit(function () {
                                        alert("修改成功!");
                                        isflag=true;
                                        return isflag;
                                    })
                                })

                                //修改_隐藏 ajax
                                $("#xg_yc_${commodity.cid}").click(function() {
                                    $("#xg_xs_${commodity.cid}").toggle();
                                })


                                $("#update_subBtn_${commodity.cid}").click(function() {
                                    $("#xg_xs_${commodity.cid}").toggle()
                                })
                            </script>
                        </div>
                    </c:if>
                        <%--商品图--%>
                        <div>
                            <a href="/xqDetails?cid=${commodity.cid}"  >
                                <img src="../statics/images/${commodity.imgpath}" alt="${commodity.cname}" width="190px" height="190px">
                                <p>
                                    <span style="color: #0c89de">${commodity.cname}</span>
                                </p>
                                    <c:if test="${commodity.discount!=null}">
                                        <p>
                                            <span style="color: black" > <s>原价: ${commodity.cmoney} </s></span>
                                        </p>
                                        <p>
                                            <span style="color: red">折后价: ${commodity.discount}</span>
                                        </p>
                                    </c:if>
                                <c:if test="${commodity.discount==null}">
                                    <p>
                                        <span style="color: black" > <s>原价: ${commodity.cmoney} </s></span>
                                    </p>
                                    <p>
                                        <span style="color: red">现价: ${commodity.cmoney}</span>
                                    </p>
                                </c:if>
                            </a>
                        </div>
                    </div>


                </c:forEach>
                <!--分页-->
                <div align="center" class="pages_div" >
                    <input type="hidden" id="totalPageCount" value="${page.pages}"/>
                    <p>共 ${page.total}件 商品 当前显示第${page.startRow}~${page.endRow}件   第${page.pageNum}/ <c:if test="${page.pages<1}">1</c:if><c:if test="${page.pages>=1}">${page.pages}</c:if>页  </p>
                    <p>
                        <c:if test="${page.isFirstPage==false}">
                            <a href="${dq_url}?pageNum=1&category=${sp_lx_id}&cname=${sp_xq}"><span>首页</span></a>
                            <a href="${dq_url}?pageNum=${page.prePage}&category=${sp_lx_id}&cname=${sp_xq}"><span>上一页</span></a>
                        </c:if>
                         <c:if test="${page.isFirstPage==true}">
                            <span>首页</span> <span>上一页</span>
                        </c:if>
                        <c:if test="${page.isLastPage==false}">
                        <a href="${dq_url}?pageNum=${page.nextPage}&category=${sp_lx_id}&cname=${sp_xq}"><span>下一页</span></a>
                        <a href="${dq_url}?pageNum=${page.pages}&category=${sp_lx_id}&cname=${sp_xq}"><span>尾页</span></a>
                        </c:if>
                        <c:if test="${page.isLastPage==true}">
                            <span>下一页</span> <span>尾页</span>
                        </c:if>
                        <span class="page-go-form"><label>跳转至</label>
                        <input type="text" name="inputPage" id="inputPage" class="page-key" value="${page.pageNum}" />页
                         <button type="button" class="page-btn" onClick='jump_to(document.getElementById("inputPage").value)'>GO</button>
                        </span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!--背景-->
<%@include file="/WEB-INF/jsp/common/foot.jsp"%>

<script src="../../statics/js/this_time.js"></script>
<%--<script src="../../statics/js/xiaoxingxing.js"></script>--%>
<script type="text/javascript">
    function jump_to(num){
        // alert(num);
        // alert(frm);
        //验证用户的输入
        var regexp=/^[1-9]\d*$/;
        var totalPageCount = document.getElementById("totalPageCount").value;
        // alert(totalPageCount);
        if(!regexp.test(num)){
            alert("请输入大于0的正整数！");
            return false;
        }else if((num-totalPageCount) > 0){
            alert("请输入小于总页数的页码");
            return false;
        }else{
            location = "${dq_url}?pageNum="+num+"&cname=${sp_xq}";
        }
    }

    $(function () {
        //查询名
        $("#cname").blur(function () {
            var username = $("#cname").val();
            //发送异步请求
            $.post("/youce_cxAjax",{cname:username},function (data) {
                if (data.cx_cname=="true"){
                    $("#cname").attr("placeholder","请输入商品名")
                }else{
                    $("#cname").attr("value",${sp_xq})
                }
            });
        });
        //查询 按钮
        $("#cx_submit").click(function () {
            var leixing =$("#category").val();
            var sptime = $("#sp_time").prop('checked');
            var spjiage = $("#sp_jiage").prop('checked');
            var paixu = $(":radio:checked").val();
            $.post("/youce_cxqita", {category:leixing,sp_time:sptime,sp_jiage:spjiage,sp_paixu:paixu},
                function (data) {
                    var leix_a_id = $("#category").val();
                    if (data.cx_category=="false"){
                        $("#category").attr("value",${sp_lx_id})
                    }
                    $(":radio").removeAttr('checked');
                    if (data.cx_sp_paixu=="1"){
                        $("#sp_shengxu").attr("checked","checked"); //默认选中升序
                    }else if (data.cx_sp_paixu=="2"){
                        $("#sp_jiangxu").attr("checked","checked"); //默认选中降序
                    }
                })
        });

    });
</script>
<%--<script src="../../statics/js/rollpage.js"></script>--%>
</body>
</html>
