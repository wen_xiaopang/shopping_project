<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>管理员管理</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- 引入 Bootstrap -->
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="../../statics/js/jquery-3.3.1.min.js"></script>
</head>
<body>
<div class="zhuti_header">
    <!--头部-->
    <%@include file="/WEB-INF/jsp/common/head.jsp"%>
</div>
<div class="container" style="position: relative;top: 40px;">
    <div class="row clearfix">
        <div class="col-md-12 column">
            <div class="page-header">
                <h1>
                    <small>查看所有管理员</small>
                </h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 column">
                        <a class="btn btn-primary" href="${pageContext.request.contextPath}/toAddMan">新增</a>
                        <a class="btn btn-primary" href="${pageContext.request.contextPath}/allManager1">显示所有管理员</a>
        </div>

        <div class="col-md-4 column"></div>

        <div class="col-md-4 column">
            <form action="${pageContext.request.contextPath}/all" method="post" style="float: right">
                <input type="text" name="mcode" class="form-control" placeholder="请输入要查询的管理员编号" >
                <input type="submit" value="查询" class="btn btn-primary">
            </form>

        </div>
    </div>
    <div class="row clearfix">
        <div class="col-md-12 column">
            <table class="table table-hover table-striped">
                <thead>
                <tr>
                    <th>管理员ID</th>
                    <th>管理员编号</th>
                    <th>密码</th>
                    <th>姓名</th>
                    <th>性别</th>
                    <th>电话</th>
<%--                    <th>地址</th>--%>
<%--                    <th>是否被核实[1,是, 0,否]</th>--%>
<%--                    <th>职务</th>--%>
<%--                    <th>员工权限</th>--%>
                </tr>
                </thead>
                <tbody>
                <%--                <c:forEach var="book" items="${requestScope.get('list')}">--%>
                <c:forEach var="man" items="${list}">
                    <tr>
                        <td>${man.mid}</td>
                        <td>${man.mcode}</td>
                        <td>${man.mpwd}</td>
                        <td>${man.mname}</td>
                        <td>
                            <c:if test="${man.mage==1}">
                            男
                            </c:if>
                            <c:if test="${man.mage==2}">
                                女
                            </c:if>
                        </td>
                        <td>${man.mphone}</td>
<%--                        <td>${man.maddress}</td>--%>
<%--                        <td>${man.verify}</td>--%>
<%--                        <td>${man.position}</td>--%>
<%--                        <td>${man.mauthority}</td>--%>

                        <td>
                            <a href="${pageContext.request.contextPath}/toUpdateMan?id=${man.mid}">更改</a> |
                            <a href="${pageContext.request.contextPath}/delMan/${man.mid}">删除</a>
                        </td>

                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!--背景-->
<%@include file="/WEB-INF/jsp/common/foot.jsp"%>