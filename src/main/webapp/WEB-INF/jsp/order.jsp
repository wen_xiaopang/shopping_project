<%--
  Created by IntelliJ IDEA.
  User: dell
  Date: 2021/3/11
  Time: 16:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <title>订单管理</title>
    <script src="../../statics/js/jquery-3.3.1.min.js"></script>
    <style type="text/css">
        .dd_zhuye{
            /*border:  1px red solid;*/
            width: 84%;
            margin: 0 auto;
            position: absolute;
            top: 40px;
            left: 8%;
        }
        .dd_biaoge{
            margin: 0 auto;
            width: 100%;
            background: rgba(230,230,250, 0.9);
            opacity: 0.7;
        }

        .pages_div{
            /*border: 1px solid black;*/
            /*position: relative;*/
            text-align: center;
            height: 30px;
            line-height: 30px;
        }
        .pages_div p span{
            border: 1px solid 	#A9A9A9;
            background: #DCDCDC;
            border-radius: 2px;
            padding: 1px 5px;
            height: 30px;
            line-height: 30px;
        }
        #inputPage2{
            width: 40px;
        }


    </style>
</head>
<body>


<div class="zhuti"  >
    <!--头部-->
    <%@include file="/WEB-INF/jsp/common/head.jsp"%>
    <div class="dd_zhuye">
        <div class="dd_biaoge">
            <c:forEach items="${dingdan}" var="order" varStatus="orderStatus">
            <!--表格1-->
            <table border="2" style="width: 100%;">
                <tr style="font-size: 20px; background: #DA70D6;">
                    <th style="width: 20%;">订单号</th>
                    <th style="width: 20%;">下单时间</th>
                    <th style="width: 20%;">发货时间</th>
                    <th style="width: 15%;">订单状态</th>
                    <th style="width: 10%;">订单总金额</th>
                    <th style="width: 15%;">操作</th>
                </tr>
                <tr style="text-align: center;">
                    <td>${order.orderCode}</td>
                    <td>
                        <fmt:formatDate value="${order.xiadantime}" pattern="yyyy-MM-dd HH:mm:ss"/>
                    </td>
                    <td>
                        <fmt:formatDate value="${order.fahuotime}" pattern="yyyy-MM-dd HH:mm:ss"/>
                    </td>
                    <%--订单状态--%>
                    <td>
                        <c:if test="${order.ostatus==1}">
                            代付款
                        </c:if>
                        <c:if test="${order.ostatus==2}">
                            代发货
                        </c:if>
                        <c:if test="${order.ostatus==3}">
                            已发货
                        </c:if>
                        <c:if test="${order.ostatus==4}">
                            已完成
                        </c:if>
                    </td>
                    <td>
                        <span id="zongjine_span${order.oid}"></span>
                        <%--订单总金额--%>
                        <div class="zjg" style="display: none">
                        <c:forEach items="${order.commodityList}" var="oamount" varStatus="oamount_sta">
                            <c:if test="${order.commodityList[oamount_sta.index].discount!=null}">
                                <input type="text" style="display: none;" id="zjg${oamount_sta.count}" name="zjg_zhi${order.oid}" value="
                            ${order.commodityList[oamount_sta.index].discount*order.order_items[oamount_sta.index].order_num}">
                            </c:if>
                            <c:if test="${order.commodityList[oamount_sta.index].discount==null}">
                                <input type="text" style="display: none;" id="zjg${oamount_sta.count}" name="zjg_zhi${order.oid}" value="
                                ${order.commodityList[oamount_sta.index].cmoney*order.order_items[oamount_sta.index].order_num}">
                            </c:if>
                        </c:forEach>
                            <script type="text/javascript">
                                // jQuery(function(){
                                $(document).ready(function() {
                                    var zjg = 0.00;
                                    $("input[name='zjg_zhi${order.oid}']").each(function () {
                                        // zjg += parseInt($(this).val());
                                        zjg += parseFloat($(this).val())
                                    })
                                    $("#zongjine_span${order.oid}").html("￥"+zjg+"元")
                                });
                                // })
                            </script>
                        </div>
                    </td>
                    <td><a href="/order/updateOrderStatus?ostatus=${order.ostatus}&oid=${order.oid}&pageNum=${page.pageNum}">  <span style="font: 14px '楷体';">
                        <c:if test="${order.ostatus==3}">
                            取消发货
                        </c:if>
                        <c:if test="${order.ostatus==2}">
                            确认发货
                        </c:if>
                    </span> </a></td>
                </tr>

                <c:forEach items="${order.commodityList}" var="commoditys" varStatus="comStatus">
                <tr style="background: #eeeeee; height: 30px; line-height: 30px;text-align: center;font: 18px bold ,'黑体'" >
                    <td>封面</td>
                    <td>商品名</td>
                    <td>价钱</td>
                    <td>数量</td>
                    <td>小计</td>
                    <td>库存</td>
                </tr>
                <tr style="text-align: center; height: 25px;line-height: 25px">

                    <td><img src="../../statics/images/${commoditys.imgpath}" alt="" style="width: 100px;height: 100px;"></td>
                    <td>${commoditys.cname}</td>
                    <td>
                        <c:if test="${commoditys.discount!=null}">
                            ￥${commoditys.discount}元
                        </c:if>
                        <c:if test="${commoditys.discount==null}">
                            ￥${commoditys.cmoney}元
                        </c:if>
                    </td>
                    <td>
                            ${order.order_items[comStatus.index].order_num}
                    </td>
                    <td>
                        <c:if test="${commoditys.discount!=null}">
                            ￥${commoditys.discount*order.order_items[comStatus.index].order_num}元
                        </c:if>
                        <c:if test="${commoditys.discount==null}">
                            ￥${commoditys.cmoney*order.order_items[comStatus.index].order_num}元
                        </c:if>
                    </td>
                    <td>${commoditys.stock}</td>
                </tr>
                </c:forEach>
            </table>
            </c:forEach>
            <!--分页-->
            <div align="center" class="pages_div" >
                <input type="hidden" id="totalPageCount" value="${page.pages}"/>
<%--                <p> 一共有${page.total}件商品购买详情  当前页共${dd_zongshu}件订单 当前显示第${page.startRow}~${page.endRow}件   第${page.pageNum}/ <c:if test="${page.pages<1}">1</c:if><c:if test="${page.pages>=1}">${page.pages}</c:if>页  </p>--%>
                <p>
                    <c:if test="${page.isFirstPage==false}">
                        <a href="${dq_url}?pageNum=1"><span>首页</span></a>
<%--                          到 时候 可能需要    oid 根据用户去查 --%>
<%--                        <a href="${dq_url}?pageNum=${page.prePage}&category=${sp_lx_id}&cname=${sp_xq}"><span>上一页</span></a>--%>
                        <a href="${dq_url}?pageNum=${page.prePage}"><span>上一页</span></a>
                    </c:if>
                    <c:if test="${page.isFirstPage==true}">
                        <span>首页</span> <span>上一页</span>
                    </c:if>
                    <c:if test="${page.isLastPage==false}">
                        <a href="${dq_url}?pageNum=${page.nextPage}"><span>下一页</span></a>
                        <a href="${dq_url}?pageNum=${page.pages}"><span>尾页</span></a>
                    </c:if>
                    <c:if test="${page.isLastPage==true}">
                        <span>下一页</span> <span>尾页</span>
                    </c:if>
                    <span class="page-go-form"><label>跳转至</label>
                        <input type="text" name="inputPage" id="inputPage2" class="page-key" value="${page.pageNum}" />页
                         <button type="button" class="page-btn" onClick='jump_to2(document.getElementById("inputPage2").value)'>GO</button>
                    </span>
                    第${page.pageNum}/ <c:if test="${page.pages<1}">1</c:if><c:if test="${page.pages>=1}">${page.pages}</c:if>页
                </p>
                <script type="text/javascript">
                    function jump_to2(num){
                        //alert(num);
                        //验证用户的输入
                        var regexp=/^[1-9]\d*$/;
                        var totalPageCount = document.getElementById("totalPageCount").value;
                        //alert(totalPageCount);
                        if(!regexp.test(num)){
                            alert("请输入大于0的正整数！");
                            return false;
                        }else if((num-totalPageCount) > 0){
                            alert("请输入小于总页数的页码");
                            return false;
                        }else{
                            location = "${dq_url}?pageNum="+num;
                        }
                    }
                </script>
            </div>


        </div>
    </div>


</div>
<div class="zhuye_b">
    &nbsp;
</div>
<!--背景-->
<%@include file="/WEB-INF/jsp/common/foot.jsp"%>
</body>
<!--蜘蛛网-->
<script type="text/javascript" src="../../statics/js/zhizhuwang.js"></script>
</html>
