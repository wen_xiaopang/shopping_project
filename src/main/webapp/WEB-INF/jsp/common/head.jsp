<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>	
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <script type="text/javascript" src="../../statics/js/jquery-3.3.1.min.js"></script>
    <style type="text/css" rel="stylesheet">
        /*样式初始化*/
        *{margin:0;padding: 0;}
        a{text-decoration: none;color: #000;}
        img {border: 0; vertical-align:middle;}
        ul li{list-style: none;}
        a:hover{
            color:red;
            /*text-decoration:underline;*/
        }
        .publicHeader{
            background: linear-gradient(#70c2f4,#3a8dc1, #4696c7,#83d1f5);
            /*border: 1px solid red;*/
            width: 100%;
            height: 40px;
            line-height: 40px;
            margin: 0 auto;
            text-align: center;
            font: 24px 楷体,Times New Roman;
            position:fixed;
            top: 0px;
            z-index: 999;
        }
        .publicHeaderR{
            margin: 0 auto;
            width: 80%;
            height: 38px;
            line-height: 38px;
            text-align:center;
            /*padding: 0 30px 0 0;*/
            vertical-align:middle;
        }
        .header_ul1 li{
            display: inline-block;
        }

        .header_ul li{
            background: #dedede;
            font: 16px '楷体';
            height: 30px;
            line-height: 30px;
        }
        .header_ul li:hover{
            cursor: pointer;
            background: white;
        }
        .header_ul_li_span:hover{
            color:red;
            cursor: pointer;
        }
    </style>
</head>
<!--头部-->
<header class="publicHeader">
    <div class="publicHeaderR">
        <c:if test="${adminSession!=null}">
        <div class="publicHeaderR_div">
            <ul class="header_ul1" >
                <li  style="float: left;"><b>欢乐购管理系统</b> &nbsp;</li>
                <li style="float: left;"><a href="/getAllSP">商品管理</a> &nbsp;</li>
                <li style="float: left;" id="yc_renshi"> <span  class="header_ul_li_span">人事管理</span> &nbsp;</li>
                <li style="float: left;"><a href="/order/showDingdan">订单管理</a> &nbsp;</li>
                <li style="float: right"><a href="/reg.html">注册</a>&nbsp;</li>
                <li style="float: right"><a href="/logout.html">退出</a>&nbsp;</li>
                <li style="float: right"><a href=""  >
                    欢迎 <span style="color: red"> ${adminSession.mcode} </span>[访问]
                </a>&nbsp;</li>
                <li>&nbsp;</li>
            </ul>
        </div>
        <div id="xs_renshi" style="display: none; width: 120px; position:fixed;left: 450px;top: 40px;">
            <ul class="header_ul"  >
                <li><a href="/allManager1">管理员管理</a></li>
                <li><a href="/allUser1">用户管理</a></li>
            </ul>
        </div>
        </c:if>
        <c:if test="${adminSession==null}">
            <div class="publicHeaderR">
                <ul class="header_ul1" >
                    <li  style="float: left;"><b>欢乐购商城</b> &nbsp;</li>
                    <li style="float: left;"><a href="/getAllSP">商城首页</a> &nbsp;</li>
                    <li style="float: left;"><a href="/getAllSP">我的购物车</a> &nbsp;</li>
                    <li style="float: left;"><a href="/getAllSP">我的订单</a> &nbsp;</li>
                    <li style="float: right"><a href="/reg.html">注册</a>&nbsp;</li>
                    <c:if test="${userSession!=null}">
                    <li style="float: right"><a href="/logout.html">退出</a>&nbsp;</li>
                    </c:if>
                    <li style="float: right"><a href="/login.html" >
                        <c:if test="${userSession==null}">
                            登陆 &nbsp;
                        </c:if>
                        <c:if test="${userSession!=null}">
                            欢迎 <span style="color: DarkBlue"> ${userSession.username} </span>[访问]
                        </c:if>
                    </a>&nbsp;</li>
                    <li>&nbsp;</li>
                </ul>
            </div>
        </c:if>
    </div>
</header>
    <script type="text/javascript" >
        //修改_隐藏 ajax
        $("#yc_renshi").click(function() {
            $("#xs_renshi").toggle();
        })
    </script>