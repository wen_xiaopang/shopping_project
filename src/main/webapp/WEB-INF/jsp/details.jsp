<%--
  Created by IntelliJ IDEA.
  User: dell
  Date: 2021/1/1
  Time: 15:49
  To change this template use File | Settings | File Templates.
--%>
<%--<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
    <title>商品详情</title>
    <script src="../../statics/js/jquery-3.3.1.min.js"></script>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/statics/css/xiangqing.css" />
    <style type="text/css">
        /*添加 商品 css*/
        .tianjia_shangpin{
            display: none;
            position: fixed;
            top: 100px;
            left: 500px;
            width: 370px;
            height: 460px;
            background: #E6E6E6;
            border-radius:5px;
            box-shadow:4px 4px 5px #999;
            z-index: 4;
        }


        .tianjia_shangpin h2{
            /*font: 700 16px Arial,"microsoft yahei";*/
            /*border: 1px red solid;*/
            padding: 3px 5px;
            margin: 5px 15px ;
        }
        .inputbox {
            /*border: 1px red solid;*/
            padding: 0 0 0 20px;
            margin: 2px 5px;
            height: 40px;
            line-height: 40px;
        }
        .inputbox2 {
            /*border: 1px red solid;*/
            padding: 0 0 0 20px;
            margin: 2px 5px;
            height: 100px;
            line-height: 100px;
        }
        .inputbox input,textarea {
            width: 60%;
            padding: 8px 5px 8px 20px;
            border: 1px solid rgb(178, 178, 178);
            border-radius: 3px;
            -webkit-box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.6) inset;
            -moz-box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.6) inset;
            box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.6) inset;
        }
        .insert_f_sp_js{
            position: relative;
            top: -70px;
        }
        .subBtn{
            margin: 5px auto;
            padding: 5px 0 5px 20px;
            width: 130px;
            height: 50px;
            line-height: 50px;
            /*border: 1px red solid;*/
        }
        .subBtn_a{
            position: relative;
            display: inline-block;
            background: #90EE90;
            border: 1px solid #008000;
            border-radius: 4px;
            padding: 4px 12px;
            overflow: hidden;
            color: black;
            text-decoration: none;
            text-indent: 0;
            line-height: 20px;
        }
        .subBtn_b{
            position: relative;
            display: inline-block;
            background: #FF6347;
            border: 1px solid #8B0000;
            border-radius: 4px;
            padding: 4px 12px;
            overflow: hidden;
            color: white;
            text-decoration: none;
            text-indent: 0;
            line-height: 20px;
        }

        #xg_yc{
            background: #008000;
            font: normal 16px '楷体';
            float: right;
            padding: 3px 7px;
            margin: 1px 2px;
            border-radius: 5px;
            color: #eeeeee;
        }
        #xg_yc:hover{
            cursor: pointer;
        }
    </style>
</head>
<body>
<div class="zhuti">
    <!--头部-->
    <%@include file="/WEB-INF/jsp/common/head.jsp"%>
    <div class="sp_lx_name" >
<%--                        类别 >  商品名 --%>
        <p ><a href="/getAllSP"> 首页</a> >
            <a href="/shopping_show.html?category=${commodity_xq.category}&cname=${commodity_xq.cname}">
                <c:choose>
                    <c:when test="${commodity_xq.category==1}">套装</c:when>
                    <c:when test="${commodity_xq.category==2}">春秋款</c:when>
                    <c:when test="${commodity_xq.category==3}">秋冬款</c:when>
                    <c:when test="${commodity_xq.category==4}">夏季款</c:when>
                    <c:when test="${commodity_xq.category==5}">卫衣/衬衫</c:when>
                    <c:when test="${commodity_xq.category==6}">夹克/外套</c:when>
                    <c:when test="${commodity_xq.category==7}">T恤/裤子</c:when>
                    <c:when test="${commodity_xq.category==8}">皮革</c:when>
                    <c:when test="${commodity_xq.category==9}">童装</c:when>
                </c:choose>
            </a>
            > <a href="/xqDetails?cid=${commodity_xq.cid}"> ${commodity_xq.cname}</a>
            <c:if test="${adminSession!=null}">
                 <span class="xg_yc" id="xg_yc"  name="xg_yc" >
                                    修改
                  <input type="text" style="display: none;" name="xg_yc_id" id="xg_yc_id" value="${commodity_xq.cid}">
            </span>
            </c:if>
        </p>
    </div>
<c:if test="${adminSession!=null}">
    <!--修改商品的 div-->
    <div class="tianjia_shangpin" id="xg_xs">
        <h2 >修改商品</h2>
        <hr >
        <div class="insert_f">
            <form id="from_3" action="${pageContext.request.contextPath}/updateComS?cid=${commodity_xq.cid}" method="post">
                <%--                                <div class="info" align="center">${error }</div>--%>
                <div class="inputbox">
                    <label for="update_f_sp_lx">商品类型:</label>

                    <input type="text" class="input-text" list="update_typelist" id="update_f_sp_lx" name="update_f_sp_lx" placeholder="当前商品类型:${commodity_xq.category}" />
                    <datalist id="update_typelist">
                        <c:forEach items="${leixing}" var="lx_name" varStatus="leixing_list">
                            <option value="${lx_name.category}"><c:out value="${lx_name.category_name}"/></option>
                        </c:forEach>
                    </datalist>
                </div>
                <div class="inputbox">
                    <label for="update_f_sp_mc">商品名称:</label>
                    <input type="text" class="input-text" id="update_f_sp_mc" name="update_f_sp_mc" placeholder="商品名称" value="${commodity_xq.cname}" />
                </div>
                <div class="inputbox">
                    <label for="update_f_sp_jg">商品原价:</label>
                    <input type="text" class="input-text" id="update_f_sp_jg" name="update_f_sp_jg" placeholder="商品原价" value="${commodity_xq.cmoney}"/>
                </div>
                <div class="inputbox">
                    <label for="update_f_sp_jg">商品现价:</label>
                    <input type="text" class="input-text" id="update_f_sp_zhj" name="update_f_sp_zhj" placeholder="商品现价" value="${commodity_xq.discount}"/>
                </div>
                <div class="inputbox">
                    <label for="update_f_sp_kc">商品库存:</label>
                    <input type="text" class="input-text" id="update_f_sp_kc" name="update_f_sp_kc" placeholder="商品库存" value="${commodity_xq.stock}"/>
                </div>
                <div class="inputbox">
                    <label for="update_f_sp_fm">商品封面:</label>
                    <input type="file" class="input-text" id="update_f_sp_fm" name="update_f_sp_fm" placeholder="商品封面" value="${commodity_xq.imgpath}"/>
                </div>
                <div class="inputbox2">
                    <label for="update_f_sp_js" class="insert_f_sp_js">商品介绍:</label>
                    <textarea cols="31" rows="5" id="update_f_sp_js" name="update_f_sp_js" placeholder="${commodity_xq.introduction}" ></textarea>
                    <!--            <input type="text" class="input-text" id="insert_f_sp_js" name="mCode" placeholder="商品介绍" />-->
                </div>
                <div class="subBtn">
                    <input type="submit" class="subBtn_a" value="修改"/>
                    <input type="button"  class="subBtn_b" id="update_subBtn" value="取消"/>
                </div>
            </form>
        </div>
        <script type="text/javascript" >
            //添加判断
            jQuery(function ($) {
                var isflag = false;
                $("#from_3").submit(function () {
                        alert("修改成功!");
                        isflag=true;
                        return isflag;
                })
            })

            //修改_隐藏 ajax
            $("#xg_yc").click(function() {
                $("#xg_xs").toggle();
            })


            $("#update_subBtn").click(function() {
                $("#xg_xs").toggle()
            })
        </script>
    </div>
</c:if>
    <div class="zhuye" >
        <!--左侧 主图显示-->
        <div class="s_box" >
            <img src="../../statics/images/${commodity_xq.imgpath}" alt="">
            <span></span>
        </div>
        <div class="b_box" >
            <img src="../../statics/images/${commodity_xq.imgpath}" alt="">
        </div>
        <ul class="list" >
            <li><img src="../../statics/images/${commodity_xq.imgpath}" alt=""></li>
            <li><img src="../../statics/images/t_002.jpg" alt=""></li>
            <li><img src="../../statics/images/t_003.jpg" alt=""></li>
            <li><img src="../../statics/images/t_004.jpg" alt=""></li>
            <li><img src="../../statics/images/t_005.jpg" alt=""></li>
        </ul>
        </div>
<%--    右侧 详情 介绍--%>
    <div class="sp_xq" >
        <!--右侧商品详情-->
        <!--            商品名-->
        <div class="xq_biaotou">
            <p>${commodity_xq.cname}</p>
        </div>
        <!--            商品介绍-->
        <div class="xq_jieshao"><p>${commodity_xq.introduction}</p></div>
        <!--        商品价格-->
        <div class="xq_jiage">
            <p > 原价 &nbsp;&nbsp; <span style="text-decoration:line-through">￥${commodity_xq.cmoney}</span> <br>
            <br> 现价 &nbsp;&nbsp; <span>￥
                    <c:if test="${commodity_xq.discount==null}">
                        <c:out value="${commodity_xq.cmoney}"/>
                    </c:if>
                    <c:if test="${commodity_xq.discount!=null}">
                        <c:out value="${commodity_xq.discount}"/>
                    </c:if>
                </span> </p>
        </div>
        <!--        商品 大小-->
        <div class="sp_ul">
            <ul class="sp_ul_1">
                <span>尺码</span>
                <li>S</li>
                <li>M</li>
                <li>L</li>
                <li>XL</li>
                <li>XXL</li>
                <li>3XL</li>
            </ul>
        </div>
        <!--        商品颜色-->
        <div class="sp_ul">
            <ul class="sp_ul_2">
                <span>颜色</span>
                <li>酒红色</li>
                <li>酷黑色</li>
                <li>米白色</li>
                <li>淡蓝色</li>
                <li>渐变色</li>
            </ul>
        </div>
        <!--        商品数量 库存 购买个数 加入 购物车-->
        <div class="sp_jiajian">
            <ul class="btn-numbox">
                <li><span class="number">数量 &nbsp;</span></li>
                <li>
                    <ul class="count">
                        <li><span id="num-jian" class="num-jian">-</span></li>
                        <li><input type="text" class="input-num" id="input-num"  onkeyup="value=value.replace(/[^\d]/g,'')" data-value="0" value="1" /></li>
                        <li><span id="num-jia" class="num-jia">+</span></li>
                    </ul>
                </li>
                <li><span class="kucun">（库存: ${commodity_xq.stock} ）</span></li>
                <li class="num-a">
<%--                                                                         传送地址                                               --%>
                    <a id="addCart" name="buy01" class="btn-orange-buy" href="javascript:Cart.addCart();">加入购物车</a>
                </li>
            </ul>
        </div>
    </div>


    </div>
<div class="zhuye_b">
    &nbsp;
</div>
<!--背景-->
<%@include file="/WEB-INF/jsp/common/foot.jsp"%>
<!-- 购物 边框 换颜色 -->
<script type="text/javascript" src="../../statics/js/bkhs.js"></script>
<!--放大镜-->
<script type="text/javascript" src="../../statics/js/fangdajing.js"></script>
<!-- 购买 数量 加减运算-->
<script type="text/javascript" src="../../statics/js/jiajianyunsuan.js"></script>
</body>
</html>
