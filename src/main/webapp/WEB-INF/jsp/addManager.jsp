<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
    <title>新增管理员</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <%--    引入Bootstrap--%>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <script type="text/javascript" src="../../statics/js/jquery-3.3.1.min.js"></script>
    <script>
        $(document).ready(function () {
            var mcode=null;
            var mname=null;
            var mpwd=null;
            var mage=0;
            var mphone=null;
            var maddress=null;
            var verify=0;
            var mauthority=0;

            var isFlag=false;

            var a=0;
            var b=0;
            var c=0;
            var d=0;
            var e=0;
            var f=0;


            // 员工编号判断
            $("#mcode").blur(function () {
                var path=$("#path").val();
                mcode=$("#mcode").val();
                if (mcode==null||mcode==""){
                    $("#a1:first").html("管理员编号不能为空").css("color","red");
                }else {
                    $.get(path+"/tomcode",{mcode:mcode},function (data) {
                        if(data.msg=="e"){
                           $("#a1:first").html("已经存在").css("color","red");
                       }else if(data.msg=="s"){
                           $("#a1:first").html("可以使用").css("color","green");
                           a=1;
                       }
                    },"json")
                }
            });


            // 密码判断
            $("#mpwd").blur(function () {
                mpwd=$("#mpwd").val();
                if (mpwd==null||mpwd==""){
                    $("#a2:first").html("密码不能为空").css("color","red");
                }else {
                    if (mpwd.length<6 || mpwd.length>12){
                        $("#a2:first").html("密码要在6-12位之间").css("color","red");
                    }else {
                        $("#a2:first").html("√√√").css("color","green");
                        b=1;
                    }
                }
            });


            //确认密码判断
            $("#mpwd1").blur(function () {
                mpwd1=$("#mpwd1").val();
                if (mpwd1==null||mpwd1==""){
                    $("#a3:first").html("密码不能为空").css("color","red");
                }else {
                    if (mpwd!=mpwd1){
                        $("#a3:first").html("两次密码不一致,请重新输入").css("color","red");
                    }else {
                        $("#a3:first").html("√√√").css("color","green");
                        c=1;
                    }
                }
            });

            //姓名判断
            $("#mname").blur(function () {
                mname=$("#mname").val();
                // 用JS验证用户输入的用户名只能是汉字并且在2-4位之间
                var reg = /^[\u4E00-\u9FA5]{2,4}$/;
                if(!reg.test(mname)){
                    $("#a4:first").html("请输入正确的姓名").css("color","red");
                }else{
                    $("#a4:first").html("√√√").css("color","green");
                    d=1;
                }
            })

            //性别判断
            $("#mage").blur(function () {
                mage=$("#mage").val();
                if (mage==1||mage==2){
                    e=1;
                }
            })


            //判断手机号格式
            $("#mphone").blur(function () {
                mphone=$("#mphone").val();
                // 以13、4...开头的，后边必须是9位
                if (!(/^1(3|4|5|7|8)\d{9}$/.test(mphone))){
                    $("#a5:first").html("请输入正确的手机号").css("color","red");
                }else {
                    $("#a5:first").html("√√√").css("color","green");
                    f=1;
                }
            })



            $("form").submit(function () {
                if (a==1&&b==1&&c==1&&d==1&&e==1&&f==1){
                    isFlag=true;
                }else {
                    alert("请填写完整")
                }
                return isFlag;
            })

        });
    </script>
    <style type="text/css">
        .inputbox {
            /*border: 1px red solid;*/
            /*padding: 0 0 0 30px;*/
            margin: 2px 5px;
            height: 50px;
            line-height: 50px;
        }
        .inputbox label{
            font:  18px "楷体";
        }
        .inputbox input,select {
            width: 210px;
            height: 40px;
            padding: 8px 5px 8px 20px;
            margin: 5px;
            border: 1px solid rgb(178, 178, 178);
            border-radius: 3px;
            -webkit-box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.6) inset;
            -moz-box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.6) inset;
            box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.6) inset;
        }
        .subBtn{
            /*margin: 5px auto;*/
            padding: 5px 0 5px 20px;
            width: 160px;
            height: 50px;
            line-height: 50px;
            /*border: 1px red solid;*/
        }
        .subBtn_a{
            /*position: relative;*/
            display: inline-block;
            background: #90EE90;
            border: 1px solid #008000;
            border-radius: 4px;
            padding: 4px 12px;
            overflow: hidden;
            color: black;
            text-decoration: none;
            text-indent: 0;
            line-height: 20px;
        }
        .subBtn_b{
            /*position: relative;*/
            display: inline-block;
            background: #FF6347;
            border: 1px solid #8B0000;
            border-radius: 4px;
            padding: 4px 12px;
            overflow: hidden;
            color: white;
            text-decoration: none;
            text-indent: 0;
            line-height: 20px;
        }
    </style>
</head>
<div class="zhuti_header">
    <!--头部-->
    <%@include file="/WEB-INF/jsp/common/head.jsp"%>
</div>
<div class="container" style="position: relative;top: 40px;">
    <div class="row clearfix">
        <div class="col-md-12 column">
            <div class="page-header">
                <h1>
                    <small>新增管理员</small>
                </h1>
            </div>
        </div>
    </div>
    <input type="hidden" value="${pageContext.request.contextPath}" id="path">
    <form action="${pageContext.request.contextPath}/addMan" method="get">
        <div class="inputbox">
            <label for="mcode">管理员昵称：</label>
            <input type="text" class="input-text" id="mcode" name="mcode" placeholder="请输入管理员昵称" /><span id="a1"></span><br><br><br>
        </div>
        <div class="inputbox">
            <label for="mpwd">管理员密码：</label>
            <input type="password" class="input-text" id="mpwd" name="mpwd" placeholder="请设置6-12位密码" /><span id="a2"></span><br><br><br>
        </div>
        <div class="inputbox">
            <label for="mpwd1">确认密码：</label>
            <input type="password" class="input-text" id="mpwd1"  placeholder="请确认密码" /><span id="a3"></span><br><br><br>
        </div>
        <div class="inputbox">
            <label for="mname">真实姓名：</label>
            <input type="text" class="input-text" id="mname"  name="mname" placeholder="请输入姓名" /><span id="a4"></span><br><br><br>
        </div>
        <div class="inputbox">
            <label for="mage">选择性别：</label>
            <select name="mage" id="mage">
                <option selected>--请选择--</option>
                <option value="1">------男------</option>
                <option value="2">------女------</option>
            </select><br><br><br>
        </div>
        <div class="inputbox">
            <label for="mphone">手机号码：</label>
            <input type="text" class="input-text" id="mphone" name="mphone" placeholder="请输入11位" /><span id="a5"></span><br><br><br>
        </div>
        <div class="subBtn">
            <input type="submit" class="subBtn_a" value="添加"/>
            <input type="reset" class="subBtn_b" value="重置">
        </div>
    </form>

</div>
<!--背景-->
<%@include file="/WEB-INF/jsp/common/foot.jsp"%>