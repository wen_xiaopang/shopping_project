<%--
  Created by IntelliJ IDEA.
  User: dell
  Date: 2020/12/19
  Time: 10:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>登陆</title>
    <script type="text/javascript" src="statics/js/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="statics/css/login.css">
</head>
<script type="text/javascript" >
    jQuery(function () {
        var isflag = false;
        $("form").submit(function () {
            let mCode= $("#mCode").val();
            let mPwd = $("#mPwd").val();
            if (mCode==""||mCode==null){
                alert("请输入用户名!");
            }else
            if (mPwd==""||mPwd==null){
                alert("请输入密码!")
            }else{
                isflag = true;
            }
            return isflag;
        });
    })
</script>
<body>
<%--<body style="background: url('statics/images/login_bg.jpg') 0 0 no-repeat;--%>
<%--background-size:cover;--%>
<%--margin: 0; padding:0;">--%>
<div>
    <div class="login">
        <div class="login_h">
            <h2>欢乐购管理系统</h2>
        </div>
        <div class="login_f">
            <form action="${pageContext.request.contextPath}/doLogin" method="post">
                <div class="info">${error }</div>
                <div class="inputbox">
                    <label for="mCode">用户名：</label>
                    <input type="text" class="input-text" id="mCode" name="mCode" placeholder="请输入用户名" />
                </div>
                <div class="inputbox">
                    <label for="mPwd">密&nbsp;&nbsp;&nbsp;码：</label>
                    <input type="password" id="mPwd" name="mPwd" placeholder="请输入密码" />
                </div>
                <div class="inputbox2">
                    <input type="radio" id="login_radio1" name="login_radio" value="0" checked>
                    <label for="login_radio1"> 用户</label>
                    </input>
                    <input type="radio" id="login_radio2" name="login_radio" value="1">
                    <label for="login_radio2"> 管理员</label>
                    </input> <br>
                </div>
                <div class="subBtn">
                    <input type="submit" value="登录"/>
                    <input type="reset" value="重置"/>
                </div>
            </form>
        </div>
    </div>
</div>
<!--背景-->
<%@include file="/WEB-INF/jsp/common/foot.jsp"%>
</body>
</html>
