package com.wx.controller.commodity;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wx.bean.Commodity;
import com.wx.bean.commodity.Commodity_Category;
import com.wx.service.commodity.CommodityService;
import com.wx.tools.Constants;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionUsageException;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class commodityController {
    @Autowired
    private CommodityService commodityService;


    //首页 各种查询
    @RequestMapping("/shopping_show.html")
    public String sp_show(Model model,@RequestParam(value = "pageNum", required = false) Integer pageNum,
                          @RequestParam(value = "category", required = false) Integer category,
                          HttpSession session){
        if (session.getAttribute("sp_paixu")==null){session.setAttribute(Constants.SP_PAIXU,1);}
        if (session.getAttribute("sp_cxtime")==null){session.setAttribute(Constants.SP_CXTIME, true);}
        List<Commodity_Category> leiXingAll = commodityService.getLeiXingAll();
        session.setAttribute(Constants.COMMODITY_LEIXING,leiXingAll);
        session.setAttribute(Constants.DQ_URL,"/shopping_show.html");
        if (pageNum==null||pageNum.equals("")){ pageNum=1; }
        PageHelper.startPage(pageNum,10);
        if (category!=null){
            session.setAttribute(Constants.SP_LX_ID,category);
        }
        List<Commodity> all = null;
        if (session.getAttribute("sp_paixu").equals("2")){//降序
            if (session.getAttribute("sp_cxtime").equals("true") && session.getAttribute("sp_cxjiage").equals("true") ){//两个选中
                all = commodityService.getAllByLzDESC((Integer) session.getAttribute("sp_lx_id"),(String)session.getAttribute("sp_xq"));
            }else if (session.getAttribute("sp_cxtime").equals("false") && session.getAttribute("sp_cxjiage").equals("true")){//选中价钱
                all = commodityService.getAllByjiageDESC((Integer) session.getAttribute("sp_lx_id"),(String)session.getAttribute("sp_xq"));
            }else if (session.getAttribute("sp_cxtime").equals("true") && session.getAttribute("sp_cxjiage").equals("false")){//选中时间
                all = commodityService.getAllByTimeDESC((Integer) session.getAttribute("sp_lx_id"),(String)session.getAttribute("sp_xq"));
            }else{
                all = commodityService.getAllByIDDESC((Integer) session.getAttribute("sp_lx_id"),(String)session.getAttribute("sp_xq"));
            }
        }else{//否则升序
            if (session.getAttribute("sp_cxtime").equals("true") && session.getAttribute("sp_cxjiage").equals("true") ){//两个选中
                all = commodityService.getAllByLzASC((Integer) session.getAttribute("sp_lx_id"),(String)session.getAttribute("sp_xq"));
            }else if (session.getAttribute("sp_cxtime").equals("false") && session.getAttribute("sp_cxjiage").equals("true")){//选中价钱
                all = commodityService.getAllByjiageASC((Integer) session.getAttribute("sp_lx_id"),(String)session.getAttribute("sp_xq"));
            }else if (session.getAttribute("sp_cxtime").equals("true") && session.getAttribute("sp_cxjiage").equals("false")){//选中时间
                all = commodityService.getAllbyCategory((Integer) session.getAttribute("sp_lx_id"),(String)session.getAttribute("sp_xq"));
            }else{
                all = commodityService.getAllByIDASC((Integer) session.getAttribute("sp_lx_id"),(String)session.getAttribute("sp_xq"));
            }
        }
        //获取与分页相关的参数
        PageInfo<Commodity> pageInfo = new PageInfo<Commodity>(all);

//        //获取与分页相关的参数
//        System.out.println("当前页:"+pageInfo.getPageNum());
//        System.out.println("每页显示条数:"+pageInfo.getPageSize());
//        System.out.println("总条数:"+pageInfo.getTotal());
//        System.out.println("总页数:"+pageInfo.getPages());
//        System.out.println("上一页:"+pageInfo.getPrePage());
//        System.out.println("下一页:"+pageInfo.getNextPage());
//        System.out.println("首页:"+pageInfo.getFirstPage());
//        System.out.println("尾页:"+pageInfo.getLastPage());
//        System.out.println("是否是第一页:"+pageInfo.isIsFirstPage());
//        System.out.println("是否是最后一页:"+pageInfo.isIsLastPage());


        model.addAttribute("page",pageInfo);
        model.addAttribute("shopping",all);
        return "frame";
    }

    //清楚类型
    @RequestMapping("/getAllSP")
    public String getAllSP(HttpSession session){
        //清楚类型session
        session.removeAttribute(Constants.SP_LX_ID);
        return "redirect:/shopping_show.html";
    }

    //上下架
    @RequestMapping("/updateSXjia")
    public String updateSXjia(@RequestParam(value = "cstatus",required = false) Integer cstatus,
                              @RequestParam(value = "cid",required = false)Integer cid,
                              @RequestParam(value = "pageNum",required = false)Integer pageNum
                            ,HttpSession session ){
        if (cstatus==1){
            boolean b = commodityService.updateCstatusByid(0, cid);
        }else if (cstatus==0){
            boolean b = commodityService.updateCstatusByid(1, cid);
        }
        return "redirect:"+session.getAttribute("dq_url")+"?pageNum="+pageNum;
    }

    //根据 ID 查询单个商品
    @RequestMapping("/xqDetails")
    public String xqDetails(@RequestParam(value = "cid",required = false)Integer cid,
                            HttpSession session,Model model){
        session.setAttribute(Constants.DQ_URL,"/xqDetails");
        Commodity commodityByID = commodityService.getCommodityByID(cid);
        model.addAttribute("commodity_xq",commodityByID);
        return "details";
    }


        //添加商品信息
    @RequestMapping("/addCommodity")
    public String addCommodity(@RequestParam(value = "pageNum",required = false)Integer pageNum,/*当前页*/
                               @RequestParam(value = "insert_f_sp_lx",required = false)Integer category,/*类型id*/
                               @RequestParam(value = "insert_f_sp_mc",required = false)String cname,/*名称*/
                               @RequestParam(value = "insert_f_sp_jg",required = false)Double cmoney,/*价格*/
                               @RequestParam(value = "insert_f_sp_kc",required = false)Integer stock,/*库存*/
                               @RequestParam(value = "insert_f_sp_fm",required = false)String imgpath,/*封面*/
                               @RequestParam(value = "insert_f_sp_js",required = false)String introduction,/*介绍*/
                               HttpSession session, Model model
                               ){
        double discount = cmoney*0.8;//      折后价  discount
        Commodity c = new Commodity();
        c.setCname(cname);
        c.setCmoney(cmoney);
        c.setCategory(category);
        c.setStock(stock);
        c.setImgpath(imgpath);
        c.setIntroduction(introduction);
        c.setDiscount(discount);
        int index = commodityService.addCommodity(c);
        if (index>0){
            model.addAttribute("tj_ts", "添加成功");
        }else {
            model.addAttribute("tj_ts", "添加失败");
        }
        return  "redirect:"+session.getAttribute("dq_url")+"?pageNum="+pageNum;
    }

    //修改 商品信息
    @RequestMapping("/updateComS")
    public String updateComS(@RequestParam(value = "cid",required = false)Integer cid,
                                  @RequestParam(value = "pageNum",required = false)Integer pageNum,
                                  @RequestParam(value = "update_f_sp_lx",required = false)Integer category,/*类型id*/
                                  @RequestParam(value = "update_f_sp_mc",required = false)String cname,/*名称*/
                                  @RequestParam(value = "update_f_sp_jg",required = false)Double cmoney,/*价格*/
                                  @RequestParam(value = "update_f_sp_kc",required = false)Integer stock,/*库存*/
                                  @RequestParam(value = "update_f_sp_fm",required = false)String imgpath,/*封面*/
                                  @RequestParam(value = "update_f_sp_js",required = false)String introduction,/*介绍*/
                                  @RequestParam(value = "update_f_sp_zhj",required = false)Double discount,/*介绍*/
                                  HttpSession session,Model model) {
        //设置map集合存放接收值
//        Map<String,Object> map = new HashMap<String,Object>();
        Commodity byID = commodityService.getCommodityByID(cid);
        Commodity c = new Commodity();
        if (category==null|| category.equals("")){
            c.setCategory(byID.getCategory());//类型
        }else{
            c.setCategory(category);//类型
        }

        if (cname==null|| cname.equals("")){
            c.setCname(byID.getCname());//名称
        }else{
            c.setCname(cname);//名称
        }
        if (cmoney==null|| cmoney.equals("")){
            c.setCmoney(byID.getCmoney()); //价钱
        }else{
            c.setCmoney(cmoney); //价钱
        }
        if (stock==null|| stock.equals("")){
            c.setStock(byID.getStock()); //库存
        }else{
            c.setStock(stock); //库存
        }
        if (imgpath==null|| imgpath.equals("")){
            c.setImgpath(byID.getImgpath()); //封面
        }else{
            c.setImgpath(imgpath); //封面
        }
        if (introduction==null|| introduction.equals("")){
            c.setIntroduction(byID.getIntroduction()); //介绍
        }else{
            c.setIntroduction(introduction); //介绍
        }
        if (discount==null || discount.equals("")){
            c.setDiscount(byID.getDiscount()); //现价
        }else{
            c.setDiscount(discount); //现价
        }
        c.setCid(cid);
        int i = commodityService.updateCommodityByID(c);
        if(pageNum==null){
            return "redirect:/xqDetails?cid="+cid;
        }else{
            return "redirect:"+session.getAttribute("dq_url")+"?pageNum="+pageNum;
        }
    }
    //删除 商品信息
    @RequestMapping("/delShangPing")
    public String delShangPing(@RequestParam(value = "cid",required = false)Integer cid,
                             @RequestParam(value = "pageNum",required = false)Integer pageNum,
                             HttpSession session,Model model){
        int i = commodityService.delCommodity(cid);
        return "redirect:"+session.getAttribute("dq_url")+"?&pageNum="+pageNum;
    }

}
