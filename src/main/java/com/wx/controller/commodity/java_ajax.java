package com.wx.controller.commodity;

import com.alibaba.fastjson.JSON;
import com.wx.bean.Commodity;
import com.wx.service.commodity.CommodityService;
import com.wx.service.commodity.Impl.CommodityServiceImpl;
import com.wx.tools.Constants;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@Controller
public class java_ajax {

    @RequestMapping("/youce_cxAjax")
    public String youce_cxAjax(@Param("cname") String cname, HttpSession session){
        
        //设置map集合存放接收值
        Map<String,Object> map = new HashMap<String,Object>();
        if (cname==null || cname.equals("")){
            //清楚类型session
            session.removeAttribute(Constants.SP_XQ);
            map.put("cx_cname",true);
        }else if (cname!=null){
            session.setAttribute(Constants.SP_XQ,cname);
            map.put("cx_cname",false);
        }
        String json = JSON.toJSONString(map);
     return json;
    }

    @RequestMapping("/youce_cxqita")
    public String youce_cxQita( @RequestParam(value = "category", required = false) Integer category,
                                @RequestParam(value = "sp_paixu", required = false) String sp_paixu,
                                @RequestParam(value = "sp_time", required = false) String sp_time,
                                @RequestParam(value = "sp_jiage", required = false) String sp_jiage,
                                HttpSession session){

        session.setAttribute(Constants.SP_CXTIME,sp_time);
        session.setAttribute(Constants.SP_CXJIAGE,sp_jiage);
        session.setAttribute(Constants.SP_PAIXU,sp_paixu);
        //设置map集合存放接收值
        Map<String,Object> map = new HashMap<String,Object>();

        if (session.getAttribute("sp_paixu").equals("1")){
            map.put("cx_sp_paixu","1");
        }else
        if (session.getAttribute("sp_paixu").equals("2")){
            map.put("cx_sp_paixu","2");
        }

        if (category==null){
            //清楚类型session
            session.removeAttribute(Constants.SP_LX_ID);
            map.put("cx_category",true);
        }else if (category!=null){
            session.setAttribute(Constants.SP_LX_ID,category);
            map.put("cx_category",false);
        }
        String json = JSON.toJSONString(map);
        return json;
    }
}
