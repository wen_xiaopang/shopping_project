package com.wx.controller.test;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wx.bean.User;
import com.wx.service.personnel.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.SimpleDateFormat;
import java.util.List;

//这个就是控制层,加了注解的方法都可以称之为处理器
@Controller
//@RequestMapping("/user")
public class TestController {
    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    @Autowired
    private UserService userService;
    @RequestMapping("/admin")
    public String index(Model model,@RequestParam(value="pageNum",required=false)Integer pageNum){

        if (pageNum==null){
            pageNum=1;
        }
//        // 查询所有
        PageHelper.startPage(pageNum,3);
        List<User> list2 = userService.getAll();
        for (User user1:list2){
            System.out.println("--"+user1);
        }
//        //获取与分页相关的参数
        PageInfo<User> pageInfo = new PageInfo<User>(list2);
        System.out.println("当前页:"+pageInfo.getPageNum());
        System.out.println("每页显示条数:"+pageInfo.getPageSize());
        System.out.println("总条数:"+pageInfo.getTotal());
        System.out.println("总页数:"+pageInfo.getPages());
        System.out.println("上一页:"+pageInfo.getPrePage());
        System.out.println("下一页:"+pageInfo.getNextPage());
        System.out.println("是否是第一页:"+pageInfo.isIsFirstPage());
        System.out.println("是否是最后一页:"+pageInfo.isIsLastPage());
//
        model.addAttribute("list",list2);
        model.addAttribute("Total",pageInfo.getTotal());
        model.addAttribute("pageNum",pageInfo.getPageNum());
        model.addAttribute("Pages",pageInfo.getPages());
        return "admin";
    }
    
    
    
}
