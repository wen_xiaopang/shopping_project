package com.wx.controller.order;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wx.bean.Commodity;
import com.wx.bean.Order;
import com.wx.service.order.OrderService;
import com.wx.tools.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private OrderService orderService;

    //修改 订单状态
    @RequestMapping("/updateOrderStatus")
    public String updateSXjia(@RequestParam(value = "ostatus",required = false) Integer ostatus,
                              @RequestParam(value = "oid",required = false)Integer oid,
                              @RequestParam(value = "pageNum",required = false)Integer pageNum
            ,HttpSession session ){
        if (ostatus==3){
            boolean b = orderService.updateOstatusByoid(2, oid,null);
        }else if (ostatus==2){
            boolean b = orderService.updateOstatusByoid(3, oid,new Date());
        }
        return "redirect:"+session.getAttribute("dq_url")+"?pageNum="+pageNum;
//        return "redirect:"+session.getAttribute("dq_url");
    }


    //订单分页查询
    @RequestMapping("/showDingdan")
    public String showDingdan(@RequestParam(value = "pageNum", required = false) Integer pageNum,

            Model model, HttpSession session){
        session.setAttribute(Constants.DQ_URL,"/order/showDingdan");
        if (pageNum==null||pageNum.equals("")){ pageNum=1; }
        PageHelper.startPage(pageNum,10);
        List<Order> orderAll = orderService.getOrderAll();
        System.out.println(orderAll.toArray().length);
        //获取与分页相关的参数
        PageInfo<Order> pageInfo = new PageInfo<Order>(orderAll);
        model.addAttribute("page",pageInfo);
        model.addAttribute("dd_zongshu",orderAll.toArray().length);
        model.addAttribute("dingdan",orderAll);
        return "order";
    }
}
