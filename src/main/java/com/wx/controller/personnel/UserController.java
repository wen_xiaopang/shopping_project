package com.wx.controller.personnel;

import com.alibaba.fastjson.JSON;
import com.wx.bean.User;
import com.wx.service.personnel.UserService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;

@Controller
public class UserController {

    @Autowired
    private UserService userService;


    //1.查询全部
    @RequestMapping("/allUser1")
    public String list(Model model){
        List<User> list = userService.queryAllUser();
        System.out.println(list);
        model.addAttribute("list",list);
        return "allUser";
    }

    //2.添加
    @RequestMapping("/toAddUser")
    public String toAddPaper(){
        return "addUser";
    }
    @RequestMapping("addUser")
    public String AddPaper(User user){
        System.out.println("---------user---------"+user);
        userService.addUser(user);
        return "redirect:/allUser1";
    }

    //注册user
    @RequestMapping("zhuceUser")
    public String zhuceUser(User user){
        System.out.println("---------user---------"+user);
        userService.addUser(user);
        return "redirect:/login.html";
    }

    //3.改
    @RequestMapping("/toUpdateUser")
    public String toUpdateUser(Model model,int id){
        System.out.println("----1---id-----"+id+"----------");
        User user = userService.queryUserById(id);
        System.out.println("----2----user----"+user+"----------");

        model.addAttribute("user",user);
        return "updateUser";
    }
    @RequestMapping("/updateUser")
    public String updateUser(Model model,User user){
        System.out.println("------3-------"+user);
        userService.updateUser(user);
        User user1 = userService.queryUserById(user.getUid());
        System.out.println("------4-------"+user1);
        model.addAttribute("user1",user1);
        return "redirect:/allUser1";
    }

    //4.删除
    @RequestMapping("/delUser/{uid}")
    public String deleteUser(@PathVariable("uid")int id){
        int i = userService.deleteUserById(id);
        System.out.println("-----------"+i);
        return "redirect:/allUser1";
    }

    //5.模糊查询
    @RequestMapping("/all1")
    public String all(String usercode,Model model){
        System.out.println("-------模糊查询--------"+usercode);
        List<User>list = userService.all(usercode);
        list.forEach(System.out::println);
        model.addAttribute("list",list);
        return "allUser";
    }

    //      6.查询用户名
//    新增验证
//    @ResponseBody的作用其实是将java对象转为json格式的数据。
    @RequestMapping("/tousercode")
    @ResponseBody
    public Object toAddUsercode(@Param("usercode")String usercode){
        System.out.println("------usercode:------"+usercode);
        HashMap<String,String>map=new HashMap<>();
        String msg="a";
        User i = userService.queryByUsercode(usercode);
        if (i!=null){
            msg="b";
        }
        map.put("msg",msg);
        System.out.println("------通过-usercode-查出来对象------"+i);
        String json = JSON.toJSONString(map);
        return json;
    }
}
