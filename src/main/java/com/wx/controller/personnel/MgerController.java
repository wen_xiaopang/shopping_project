package com.wx.controller.personnel;

import com.alibaba.fastjson.JSON;
import com.wx.bean.Manager;
import com.wx.service.personnel.ManagerService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;

@Controller
public class MgerController {
    @Autowired
    private ManagerService managerService;
    //1.查询全部
    @RequestMapping("/allManager1")
    public String list(Model model){
        List<Manager> list = managerService.queryAllMan();
        model.addAttribute("list",list);
        return "allManager";
    }

    //2.添加
    @RequestMapping("/toAddMan")
    public String toAddPaper(){
        return "addManager";
    }

    @RequestMapping("addMan")
    public String AddPaper(Manager manager){
        managerService.addMan(manager);
        return "redirect:/allManager1";
    }

    //3.改
    @RequestMapping("/toUpdateMan")
    public String toUpdateMan(Model model,int id){
        System.out.println("----1---id-----"+id+"----------");
        Manager manager = managerService.queryManById(id);
        System.out.println("----2----manager----"+manager+"----------");

        model.addAttribute("manager",manager);
        return "updateManager";
    }
    @RequestMapping("/updateMan")
    public String updateMan(Model model,Manager manager){
        System.out.println("------3-------"+manager);
        managerService.updateMan(manager);
        Manager manager1 = managerService.queryManById(manager.getMid());
        System.out.println("------4-------"+manager1);
        model.addAttribute("manager1",manager1);
        return "redirect:/allManager1";
    }

    //4.删除
    @RequestMapping("/delMan/{mid}")
    public String deleteMan(@PathVariable("mid") int id){
        managerService.deleteManById(id);
        return "redirect:/allManager1";
    }

    //5.模糊查询
    @RequestMapping("/all")
    public String all(String mcode,Model model){
        System.out.println("-------模糊查询--------"+mcode);
        List<Manager> list=managerService.all(mcode);
        list.forEach(System.out::println);
        model.addAttribute("list",list);
        return "allManager";
    }

    //6.查询用户名
//    新增验证
    @RequestMapping("/tomcode")
    @ResponseBody
    public Object toAddmcode(@Param("mcode") String mcode){
        System.out.println("------mcode:------"+mcode);
        HashMap<String,String> map=new HashMap<String, String>();
        String msg="s";
        Manager i = managerService.queryByMcode(mcode);
        if(i!=null){
            msg="e";
        }
        map.put("msg",msg);
        System.out.println("------通过-mcode-查出来对象------"+i);
        String json = JSON.toJSONString(map);
        return json;
    }
}
