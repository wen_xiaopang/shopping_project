package com.wx.controller.login;

import com.wx.bean.Manager;
import com.wx.bean.User;
import com.wx.bean.commodity.Commodity_Category;
import com.wx.dao.personnel.UserMapper;
import com.wx.service.commodity.CommodityService;
import com.wx.service.personnel.ManagerService;
import com.wx.tools.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.List;

@Controller
public class LoginController {
    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @Autowired
    private ManagerService managerService;
    @Autowired
    private UserMapper userMapper;


    @RequestMapping("/doLogin")
    public String doLoginShow(@RequestParam(value = "mCode",required = false) String mcode, @RequestParam(value = "mPwd",required = false) String mpwd, @RequestParam Integer login_radio, HttpSession session, Model model) {
        //判断谁要登陆
        if (login_radio == 1) {
            Manager M1 = managerService.getManagerBymcode(mcode);
            if (M1 != null) {
                if (M1.getMpwd().equals(mpwd)) {
                    System.out.println("管理员登陆成功!");
                    session.setAttribute(Constants.ADMIN_SESSION, M1);
                    return "redirect:/main.html";
                } else {
                    model.addAttribute("error", "管理员密码错误!");
                    return "login";
                }
            } else {
                model.addAttribute("error", "管理员不存在!");
                return "login";
            }
        } else {
            User U1 = userMapper.getUserByusercode(mcode);
            if(U1!=null){
                if (U1.getPwd().equals(mpwd)){
                    System.out.println("用户登陆成功!");
                    session.setAttribute(Constants.USER_SESSION, U1);
                    return "redirect:/main.html";
                }else{
                    model.addAttribute("error", "用户密码错误!");
                    return "login";
                }
            }else{
                model.addAttribute("error", "用户不存在!");
                return "login";
            }
        }
    }

    @RequestMapping("/login.html")
    public String login(){
        return "login";
    }

    @RequestMapping("/reg.html")
    public String reg(){
        return "reg";
    }

    @RequestMapping("/main.html")
    public String main(HttpSession session){
        System.out.println(session.getAttribute("adminSession")+"---adminSession-----");
        System.out.println(session.getAttribute("userSession")+"---userSession-----");
        session.setAttribute(Constants.SP_PAIXU,"1");
        session.setAttribute(Constants.SP_CXTIME,"true");
        session.setAttribute(Constants.SP_CXJIAGE,"false");
        if (session.getAttribute("adminSession")==null&&session.getAttribute("userSession")==null){
            //session没有保存用户信息,不能去主页
            return "redirect:/login.html";
        }
        return "redirect:/shopping_show.html";
    }


    @RequestMapping(value="/logout.html")
    public String logout(HttpSession session){
        //清除session
        session.removeAttribute(Constants.USER_SESSION);
        session.removeAttribute(Constants.ADMIN_SESSION);
        session.removeAttribute(Constants.SP_LX_ID);
        session.removeAttribute(Constants.DQ_URL);
        session.removeAttribute(Constants.COMMODITY_LEIXING);
        session.removeAttribute(Constants.SP_XQ);
        return "login";
    }

}
