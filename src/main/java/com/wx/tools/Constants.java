package com.wx.tools;

public class Constants {
	public final static String USER_SESSION = "userSession";  //用户登陆
	public final static String ADMIN_SESSION = "adminSession";  //管理员 登陆
	public final static String COMMODITY_LEIXING = "leixing";  //全部类型 表中数据

	public final static String SP_LX_ID = "sp_lx_id"; //类型 id 分类查询
	public final static String SP_XQ = "sp_xq";  // ajax  查询cname
	public final static String DQ_URL = "dq_url";//分页当前 地址
	public final static String SP_CXTIME = "sp_cxtime";// 查询时间
	public final static String SP_CXJIAGE = "sp_cxjiage";//查询价格
	public final static String SP_PAIXU = "sp_paixu"; // 排列顺序


}
