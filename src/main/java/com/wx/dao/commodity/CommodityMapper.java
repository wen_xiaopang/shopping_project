package com.wx.dao.commodity;

import com.wx.bean.Commodity;
import com.wx.bean.commodity.Commodity_Category;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface CommodityMapper {

    //删除商品信息
    public int delCommodity(@Param("cid") Integer cid);
    //添加商品信息
    public int addCommodity( Commodity commodity);
    //修改商品信息
    public int updateCommodityByID(Commodity commodity);

    //根据商品ID查询
    public Commodity getCommodityByID(@Param("cid") Integer cid);
    //修改商品上下架状态
    public boolean updateCstatusByid(@Param("cstatus") Integer cstatus,@Param("cid") Integer cid);

    //查询商品的 信息 [commodity] 安时间升序
    public List<Commodity> getAllbyCategory(@Param("category")Integer category,@Param("cname")String cname);
    //查询商品的 信息 [commodity] 安价格升序
    public List<Commodity> getAllByjiageASC(@Param("category")Integer category,@Param("cname")String cname);
    //查询商品的 信息 [commodity] 两种升序
    public List<Commodity> getAllByLzASC(@Param("category")Integer category,@Param("cname")String cname);
    //查询商品的 信息 [commodity] 按时间降序
    public List<Commodity> getAllByTimeDESC(@Param("category")Integer category,@Param("cname")String cname);
    //查询商品的 信息 [commodity] 安价钱降序
    public List<Commodity> getAllByjiageDESC(@Param("category")Integer category,@Param("cname")String cname);
    //查询商品的 信息 [commodity] 两种降序
    public List<Commodity> getAllByLzDESC(@Param("category")Integer category,@Param("cname")String cname);
    //查询商品的 信息 [commodity] ID升序
    public List<Commodity> getAllByIDASC(@Param("category")Integer category,@Param("cname")String cname);
    //查询商品的 信息 [commodity] 按ID降序
    public List<Commodity> getAllByIDDESC(@Param("category")Integer category,@Param("cname")String cname);


    //查询所有 商品信息
    public List<Commodity> getAll();

    //查询商品的 类型 [commodity_category]
    public List<Commodity_Category> getLeiXingAll();
    //查询商品的 品牌 [commodity_brand]
    //查询商品的 颜色 [commodity_color]
    //查询商品的 大小 [commodity_size]
    //查询商品的 材料 [commodity_material]
    //查询商品的 人群 [commodity_crowd]
}
