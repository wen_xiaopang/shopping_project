package com.wx.dao.personnel;

import com.wx.bean.Manager;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

//@Repository
public interface ManagerMapper {
    //查询 所有管理员
    public List<Manager> getManagerList();
    //根据 管理员编码 查询管理员信息
    public Manager getManagerBymcode(@Param("mcode") String mcode);

    //增加
    int addMan(Manager manager);

    //根据id删除
    int deleteManById(int id);

    //修改
    int updateMan(Manager manager);

    //根据id查询，返回一个Manager
    Manager queryManById(int id);

    //查询全部Manager，返回一个list集合
    List<Manager> queryAllMan();

    //右上角模糊查询
    List<Manager> all(String mcode);

    //根据mcode查询，返回一个Manager对象
    Manager queryByMcode(String mcode);
}
