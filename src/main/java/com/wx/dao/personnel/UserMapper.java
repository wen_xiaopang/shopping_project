package com.wx.dao.personnel;


import com.wx.bean.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface UserMapper {
    //查询所有
    public List<User> getUserList();
    //根据用户 编码查询用户
    public User getUserByusercode(@Param("usercode")String usercode);

    //增加
    int addUser(User user);

    //根据id删除
    int deleteUserById(int id);

    //修改
    int updateUser(User user);

    //根据id查询，返回一个 User
    User queryUserById(int id);

    //查询全部 User，返回一个list集合
    List<User> queryAllUser();

    //右上角模糊查询
    List<User> all(String usercode);

    //根据 usercode 查询，返回一个 User 对象
    User queryByUsercode(String usercode);
}
