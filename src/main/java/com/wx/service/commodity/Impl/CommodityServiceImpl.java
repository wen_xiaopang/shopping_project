package com.wx.service.commodity.Impl;

import com.wx.bean.Commodity;
import com.wx.bean.commodity.Commodity_Category;
import com.wx.dao.commodity.CommodityMapper;
import com.wx.service.commodity.CommodityService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service("commodityService")
public class CommodityServiceImpl implements CommodityService {
    @Autowired
    private CommodityMapper commodityMapper;


    @Override
    public int delCommodity(Integer cid) {
        return commodityMapper.delCommodity(cid);
    }

    /**
     * 添加商品信息
     * @param commodity
     * @return
     */
    @Override
    public int addCommodity(Commodity commodity) {
        return commodityMapper.addCommodity(commodity);
    }

    @Override
    public int updateCommodityByID(Commodity commodity) {
        return commodityMapper.updateCommodityByID(commodity);
    }

    @Override
    public Commodity getCommodityByID(@Param("cid") Integer cid) {
        return commodityMapper.getCommodityByID(cid);
    }

    /**
     * 根据id修改 商品的上下架状态
     * @param cstatus  状态
     * @param cid   商品id
     * @return
     */
    @Override
    public boolean updateCstatusByid(@Param("cstatus") Integer cstatus, @Param("cid") Integer cid) {
        return commodityMapper.updateCstatusByid(cstatus,cid);
    }
    //查询所有商品
    @Override
    public List<Commodity> getAllbyCategory(@Param("category")Integer category,@Param("cname")String cname) {
        return commodityMapper.getAllbyCategory(category,cname);
    }

    @Override
    public List<Commodity> getAllByjiageASC(@Param("category")Integer category,@Param("cname")String cname) {
        return commodityMapper.getAllByjiageASC(category, cname);
    }

    @Override
    public List<Commodity> getAllByLzASC(@Param("category")Integer category,@Param("cname")String cname) {
        return commodityMapper.getAllByLzASC(category, cname);
    }

    @Override
    public List<Commodity> getAllByTimeDESC(@Param("category")Integer category,@Param("cname")String cname) {
        return commodityMapper.getAllByTimeDESC(category, cname);
    }

    @Override
    public List<Commodity> getAllByjiageDESC(@Param("category")Integer category,@Param("cname")String cname) {
        return commodityMapper.getAllByjiageDESC(category, cname);
    }

    @Override
    public List<Commodity> getAllByLzDESC(@Param("category")Integer category,@Param("cname")String cname) {
        return commodityMapper.getAllByLzDESC(category, cname);
    }

    @Override
    public List<Commodity> getAllByIDASC(@Param("category")Integer category,@Param("cname")String cname) {
        return commodityMapper.getAllByIDASC(category, cname);
    }

    @Override
    public List<Commodity> getAllByIDDESC(@Param("category")Integer category,@Param("cname")String cname) {
        return commodityMapper.getAllByIDDESC(category, cname);
    }

    @Override
    public List<Commodity> getAll() {
        return commodityMapper.getAll();
    }


    //查询所有商品类型
    @Override
    public List<Commodity_Category> getLeiXingAll() {
        return commodityMapper.getLeiXingAll();
    }
}
