package com.wx.service.order.Impl;

import com.wx.bean.Order;
import com.wx.dao.order.OrderMapper;
import com.wx.service.order.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service("orderService")
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderMapper orderMapper;

//    @Override
//    public boolean updateOstatusByoid(Integer ostatus, Integer oid) {
//        return orderMapper.updateOstatusByoid(ostatus,oid);
//    }

    @Override
    public boolean updateOstatusByoid(Integer ostatus, Integer oid, Date fahuotime) {
        return orderMapper.updateOstatusByoid(ostatus,oid,fahuotime);
    }

    @Override
    public List<Order> getOrderAll() {
        return orderMapper.getOrderAll();
    }
}
