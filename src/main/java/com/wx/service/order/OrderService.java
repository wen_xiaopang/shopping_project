package com.wx.service.order;

import com.wx.bean.Order;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface OrderService {

    //修改订单 已发货状态
    public boolean updateOstatusByoid(@Param("ostatus") Integer ostatus, @Param("oid") Integer oid , @Param("fahuotime") Date fahuotime);

    //查询 所有有效订单
    public List<Order> getOrderAll();
}
