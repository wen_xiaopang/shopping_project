package com.wx.service.personnel.Impl;

import com.wx.bean.Manager;
import com.wx.dao.personnel.ManagerMapper;
import com.wx.service.personnel.ManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Service("managerService")
public class ManagerServiceImpl implements ManagerService {

    @Autowired
    private ManagerMapper managerMapper;
    /**
     * 查询 所有
     * @return
     */
    @Override
    public List<Manager> getManagerList() {
        return managerMapper.getManagerList();
    }

    /**
     * 根据 管理编码 查询管理员信息
     * @param mcode
     * @return
     */
    @Override
    public Manager getManagerBymcode(@RequestParam(value = "mcode") String mcode) {
        return managerMapper.getManagerBymcode(mcode);
    }

    /**
     * 增加
     * @param manager
     * @return
     */
    @Override
    public int addMan(Manager manager) {
        return managerMapper.addMan(manager);
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    @Override
    public int deleteManById(int id) {
        return managerMapper.deleteManById(id);
    }

    /**
     * 修改
     * @param manager
     * @return
     */
    @Override
    public int updateMan(Manager manager) {
        return managerMapper.updateMan(manager);
    }

    /**
     * 根据id查询，返回一个Manager
     * @param id
     * @return
     */
    @Override
    public Manager queryManById(int id) {
        return managerMapper.queryManById(id);
    }

    /**
     * 查询全部Manager，返回一个list集合
     * @return
     */
    @Override
    public List<Manager> queryAllMan() {
        return managerMapper.queryAllMan();
    }

    /**
     * 右上角模糊查询
     * @param mcode
     * @return
     */
    @Override
    public List<Manager> all(String mcode) {
        return managerMapper.all(mcode);
    }

    /**
     * 根据mcode查询，返回一个Manager对象
     * @param mcode
     * @return
     */
    @Override
    public Manager queryByMcode(String mcode) {
        return managerMapper.queryByMcode(mcode);
    }


}
