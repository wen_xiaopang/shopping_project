package com.wx.service.personnel.Impl;

import com.wx.bean.User;
import com.wx.dao.personnel.UserMapper;
import com.wx.service.personnel.UserService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("userService")
public class UserServiceImpl implements UserService {
	@Autowired
	private UserMapper userMapper;

	@Override
	public List<User> getAll() {
		// TODO Auto-generated method stub
		return userMapper.getUserList();
	}

	@Override
	public User getUserByusercode(@Param("usercode") String usercode) {
		return userMapper.getUserByusercode(usercode);
	}


	/**
	 * 增加
	 * @param user
	 * @return
	 */

	@Override
	public int addUser(User user) {
		return userMapper.addUser(user);
	}

	/**
	 * 根据id删除
	 * @param id
	 * @return
	 */
	@Override
	public int deleteUserById(int id) {
		return userMapper.deleteUserById(id);
	}

	/**
	 * 修改
	 * @param user
	 * @return
	 */
	@Override
	public int updateUser(User user) {
		return userMapper.updateUser(user);
	}

	/**
	 * 根据id查询，返回一个Manager
	 * @param id
	 * @return
	 */
	@Override
	public User queryUserById(int id) {
		return userMapper.queryUserById(id);
	}


	/**
	 * 查询全部 User，返回一个list集合
	 * @return
	 */
	@Override
	public List<User> queryAllUser() {
		return userMapper.queryAllUser();
	}


	/**
	 * 右上角模糊查询
	 * @param usercode
	 * @return
	 */
	@Override
	public List<User> all(String usercode) {
		return userMapper.all(usercode);
	}


	/**
	 * 根据usercode查询，返回一个User对象
	 * @param usercode
	 * @return
	 */
	@Override
	public User queryByUsercode(String usercode) {
		return userMapper.queryByUsercode(usercode);
	}


}
