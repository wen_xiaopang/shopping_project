package com.wx.service.personnel;

import com.wx.bean.Manager;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

public interface ManagerService {
    //查询所有 管理员信息
    public List<Manager> getManagerList();
    //根据 管理员编码 查询管理员信息
    public Manager getManagerBymcode(@Param("mcode") String mcode);


    //增加
    int addMan(Manager manager);

    //根据id删除
    int deleteManById(int id);

    //修改
    int updateMan(Manager manager);

    //根据id查询，返回一个Manager
    Manager queryManById(int id);

    //查询全部Manager，返回一个list集合
    List<Manager> queryAllMan();

    //右上角模糊查询
    List<Manager> all(String mcode);

    //根据mcode查询，返回一个Manager对象
    Manager queryByMcode(String mcode);
}
