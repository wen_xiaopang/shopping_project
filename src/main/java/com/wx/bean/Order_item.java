package com.wx.bean;

public class Order_item {

    private int order_id_xq;
    private int commodity_id_xq;
    private int order_num;

    @Override
    public String toString() {
        return "Order_item{" +
                "order_id_xq=" + order_id_xq +
                ", commodity_id_xq=" + commodity_id_xq +
                ", order_num=" + order_num +
                '}';
    }

    public int getOrder_id_xq() {
        return order_id_xq;
    }

    public void setOrder_id_xq(int order_id_xq) {
        this.order_id_xq = order_id_xq;
    }

    public int getCommodity_id_xq() {
        return commodity_id_xq;
    }

    public void setCommodity_id_xq(int commodity_id_xq) {
        this.commodity_id_xq = commodity_id_xq;
    }

    public int getOrder_num() {
        return order_num;
    }

    public void setOrder_num(int order_num) {
        this.order_num = order_num;
    }
}
