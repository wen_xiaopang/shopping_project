package com.wx.bean;

/**
 * 收货地址
 */
public class User_Receipt {
    private Integer rid;//收货人ID
    private String rname;//收货人姓名
    private String rphone;//手机号
    private String raddress;//地址
    private Integer r_uid;//所属用户



    @Override
    public String toString() {
        return "Receipt{" +
                "rid=" + rid +
                ", rname='" + rname + '\'' +
                ", rphone='" + rphone + '\'' +
                ", raddress='" + raddress + '\'' +
                ", r_uid=" + r_uid +
                '}';
    }
    //用户
    private User user; //一对一

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getRid() {
        return rid;
    }

    public void setRid(Integer rid) {
        this.rid = rid;
    }

    public String getRname() {
        return rname;
    }

    public void setRname(String rname) {
        this.rname = rname;
    }

    public String getRphone() {
        return rphone;
    }

    public void setRphone(String rphone) {
        this.rphone = rphone;
    }

    public String getRaddress() {
        return raddress;
    }

    public void setRaddress(String raddress) {
        this.raddress = raddress;
    }

    public Integer getR_uid() {
        return r_uid;
    }

    public void setR_uid(Integer r_uid) {
        this.r_uid = r_uid;
    }
}
