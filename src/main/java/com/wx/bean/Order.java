package com.wx.bean;

import java.util.Date;
import java.util.List;

/**
 * 订单 信息
 */
public class Order {
    private Integer oid; //订单ID√
    private String orderCode;//订单单号√
    private Date xiadantime;//下单时间√
    private Date fahuotime;//发货时间√
    private Integer ostatus;//订单状态√
    private Double oamount;//订单总金额√
    private Integer o_uid;//订单属于某用户√

//    private String imgpath;//	图片路径  √
//    private String cname;//	商品名  √
//    private Integer stock;//	库存  √
//    private Double  cmoney;//	商品单价 √
//    private Double discount;//	折后价  √

//    private int order_id_xq; //订单 id
//    private int commodity_id_xq; // 商品 id
//    private int order_num;// 购买数量

    private List<Commodity> commodityList;// 一对多  , 一个订单 多个商品信息
    private List<Order_item> order_items;// 一对多 ,多个购买数量




    public Integer getOid() {
        return oid;
    }

    public void setOid(Integer oid) {
        this.oid = oid;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public Date getXiadantime() {
        return xiadantime;
    }

    public void setXiadantime(Date xiadantime) {
        this.xiadantime = xiadantime;
    }

    public Date getFahuotime() {
        return fahuotime;
    }

    public void setFahuotime(Date fahuotime) {
        this.fahuotime = fahuotime;
    }

    public Integer getOstatus() {
        return ostatus;
    }

    public void setOstatus(Integer ostatus) {
        this.ostatus = ostatus;
    }

    public Double getOamount() {
        return oamount;
    }

    public void setOamount(Double oamount) {
        this.oamount = oamount;
    }

    public Integer getO_uid() {
        return o_uid;
    }

    public void setO_uid(Integer o_uid) {
        this.o_uid = o_uid;
    }

    public List<Commodity> getCommodityList() {
        return commodityList;
    }

    public void setCommodityList(List<Commodity> commodityList) {
        this.commodityList = commodityList;
    }

    public List<Order_item> getOrder_items() {
        return order_items;
    }

public void setOrder_items(List<Order_item> order_items) {
    this.order_items = order_items;
}

//    public int getOrder_id_xq() {
//        return order_id_xq;
//    }
//
//    public void setOrder_id_xq(int order_id_xq) {
//        this.order_id_xq = order_id_xq;
//    }
//
//    public int getCommodity_id_xq() {
//        return commodity_id_xq;
//    }
//
//    public void setCommodity_id_xq(int commodity_id_xq) {
//        this.commodity_id_xq = commodity_id_xq;
//    }
//
//    public int getOrder_num() {
//        return order_num;
//    }
//
//    public void setOrder_num(int order_num) {
//        this.order_num = order_num;
//    }

}