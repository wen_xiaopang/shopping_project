package com.wx.bean.commodity;

public class Commodity_Category {
    private Integer category;
    private String category_name;

    @Override
    public String toString() {
        return "商品类型:" +
                " 类型名=" + category_name ;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }
}
