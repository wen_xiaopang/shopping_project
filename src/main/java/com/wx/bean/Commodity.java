package com.wx.bean;

import java.util.Date;

/**
 * 商品 表
 */
public class Commodity {
    private Integer cid;//	商品id
    private String cnumber;//商品编号
    private String cname;//	商品名  √
    private Integer cstatus;//	上下架状态
    private String introduction;//	商品介绍  √
    private Integer category;//	商品类型id
    private String category_name;//商品类型名  √
    private Integer stock;//	库存  √
    private Double  cmoney;//	商品单价 √
    private Double discount;//	折后价  √
    private String imgpath;//	图片路径  √
    private Integer amount;//	销量

    private Integer brand_id;//品牌id
    private String  brand_name;//品牌名
    private Integer crowd_id;//适应人群id
    private String crowd_name;//人群名
    private Integer material_id;//材料id
    private String material_name;//材料名
    private Date status_time;//上架时间

    public Date getStatus_time() {
        return status_time;
    }

    public void setStatus_time(Date status_time) {
        this.status_time = status_time;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getCrowd_name() {
        return crowd_name;
    }

    public void setCrowd_name(String crowd_name) {
        this.crowd_name = crowd_name;
    }

    public String getMaterial_name() {
        return material_name;
    }

    public void setMaterial_name(String material_name) {
        this.material_name = material_name;
    }

    @Override
    public String toString() {
        return "Commodity{" +
                "cid=" + cid +
                ", cnumber='" + cnumber + '\'' +
                ", cname='" + cname + '\'' +
                ", cstatus=" + cstatus +
                ", introduction='" + introduction + '\'' +
                ", category=" + category +
                ", stock=" + stock +
                ", cmoney=" + cmoney +
                ", discount=" + discount +
                ", imgpath='" + imgpath + '\'' +
                ", amount=" + amount +
                ", brand_id=" + brand_id +
                ", crowd_id=" + crowd_id +
                ", material_id=" + material_id +
                '}';
    }

    public String getCnumber() {
        return cnumber;
    }

    public void setCnumber(String cnumber) {
        this.cnumber = cnumber;
    }

    public Integer getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(Integer brand_id) {
        this.brand_id = brand_id;
    }

    public Integer getCrowd_id() {
        return crowd_id;
    }

    public void setCrowd_id(Integer crowd_id) {
        this.crowd_id = crowd_id;
    }

    public Integer getMaterial_id() {
        return material_id;
    }

    public void setMaterial_id(Integer material_id) {
        this.material_id = material_id;
    }

    public Integer getCid() {
        return cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public Double getCmoney() {
        return cmoney;
    }

    public void setCmoney(Double cmoney) {
        this.cmoney = cmoney;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public Integer getCstatus() {
        return cstatus;
    }

    public void setCstatus(Integer cstatus) {
        this.cstatus = cstatus;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getImgpath() {
        return imgpath;
    }

    public void setImgpath(String imgpath) {
        this.imgpath = imgpath;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}
