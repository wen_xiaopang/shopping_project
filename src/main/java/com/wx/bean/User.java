package com.wx.bean;

import com.sun.jmx.snmp.Timestamp;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 用户信息表
 */
public class User implements Serializable {
    private Integer uid; //ID
    private String usercode;//用户编码
    private String pwd; //用户密码
    private Integer gender; //性别
    private String username;//真实姓名
    private String phone;//手机号
    private String email;//邮箱
    private String address;//地址
    private String createdBy;//创建者
    private Date creationDate;//创建时间
    private String modifyBy;//更新者
    private Date modifyDate;//更新时间
    private Integer u_rid;//用户下 收货地址
    private Integer u_oid;//用户下 的订单

    @Override
    public String toString() {
        return "User{" +
                "uid=" + uid +
                ", usercode='" + usercode + '\'' +
                ", pwd='" + pwd + '\'' +
                ", gender=" + gender +
                ", username='" + username + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", creationDate=" + creationDate +
                ", modifyBy='" + modifyBy + '\'' +
                ", modifyDate=" + modifyDate +
                ", u_rid=" + u_rid +
                ", u_oid=" + u_oid +
                '}';
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getUsercode() {
        return usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(String modifyBy) {
        this.modifyBy = modifyBy;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public Integer getU_rid() {
        return u_rid;
    }

    public void setU_rid(Integer u_rid) {
        this.u_rid = u_rid;
    }

    public Integer getU_oid() {
        return u_oid;
    }

    public void setU_oid(Integer u_oid) {
        this.u_oid = u_oid;
    }
}