package com.wx.bean;

/**
 * 管理员 表
 */
public class Manager {
    private Integer mid; //管理员ID
    private String mcode;//管理员编号
    private String mpwd;//密码
    private String mname; //姓名
    private Integer mage; //性别
    private String mphone;//电话
    private String maddress;//地址
    private Integer verify;//是否被 核实
    private Integer position;//职务
    private Integer mauthority;//员工权限

    @Override
    public String   toString() {
        return "Manager{" +
                "mid=" + mid +
                ", mcode='" + mcode + '\'' +
                ", mpwd='" + mpwd + '\'' +
                ", mname='" + mname + '\'' +
                ", mage=" + mage +
                ", mphone='" + mphone + '\'' +
                ", maddress='" + maddress + '\'' +
                ", verify=" + verify +
                ", position=" + position +
                ", mauthority=" + mauthority +
                '}';
    }

    public String getMcode() {
        return mcode;
    }

    public void setMcode(String mcode) {
        this.mcode = mcode;
    }

    public String getMpwd() {
        return mpwd;
    }

    public void setMpwd(String mpwd) {
        this.mpwd = mpwd;
    }

    public Integer getMauthority() {
        return mauthority;
    }

    public void setMauthority(Integer mauthority) {
        this.mauthority = mauthority;
    }

    public Integer getMid() {
        return mid;
    }

    public void setMid(Integer mid) {
        this.mid = mid;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public Integer getMage() {
        return mage;
    }

    public void setMage(Integer mage) {
        this.mage = mage;
    }

    public String getMphone() {
        return mphone;
    }

    public void setMphone(String mphone) {
        this.mphone = mphone;
    }

    public String getMaddress() {
        return maddress;
    }

    public void setMaddress(String maddress) {
        this.maddress = maddress;
    }

    public Integer getVerify() {
        return verify;
    }

    public void setVerify(Integer verify) {
        this.verify = verify;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }
}
